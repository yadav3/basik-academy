import React from "react";
import { Row, Col } from "react-bootstrap";
import styles from "../Otta/QuestionForm.module.css";
import Link from "next/link";
import Image from "next/image";

const Ottaheader = () => {
  return (
    <>
      <div>
        <Row className={`${styles.logo}`}>
          <Col md={12}>
            <Link href="/">
              <a className="p-3 display-inline-block">
                <Image
                  width={150}
                  height={90}
                  src="/skills_main_logo.svg"
                  alt="cvskillslogo"
                  style={{ cursor: "pointer" }}
                />
              </a>
            </Link>
          </Col>
        </Row>
      </div>
      <p
        className="m-0 text-center text-white fs-12 py-2 mb-5 mt-3"
        style={{ background: "#002eaf" }}
      >
        <svg
          stroke="currentColor"
          fill="currentColor"
          strokeWidth="0"
          viewBox="0 0 1024 1024"
          fontSize="18"
          height="1em"
          width="1em"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path d="M512 64C264.6 64 64 264.6 64 512s200.6 448 448 448 448-200.6 448-448S759.4 64 512 64zm0 820c-205.4 0-372-166.6-372-372s166.6-372 372-372 372 166.6 372 372-166.6 372-372 372z"></path>
          <path d="M686.7 638.6L544.1 535.5V288c0-4.4-3.6-8-8-8H488c-4.4 0-8 3.6-8 8v275.4c0 2.6 1.2 5 3.3 6.5l165.4 120.6c3.6 2.6 8.6 1.8 11.2-1.7l28.6-39c2.6-3.7 1.8-8.7-1.8-11.2z"></path>
        </svg>{" "}
        Your best match is just 3 minutes away!
      </p>
    </>
  );
};

export default Ottaheader;
