import { useState } from "react";
import styles from "../Otta/QuestionForm.module.css";
import LabelHeart from "../Otta/LabelHeart";
import { generate } from "shortid";
import { Form, Row, Col, Container } from "react-bootstrap";
import LabelCheck from "../Otta/LabelCheck";
import Box from "@mui/material/Box";
import { useForm } from "react-hook-form";
import { FormGroup, Label, Input } from "reactstrap";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import axios from "axios";
import { useRouter } from "next/router";

const steps = [
  "Select campaign settings",
  "Create an ad group",
  "Create an ad",
];

const questionlist = [
  {
    control_id: "f1",
    label: <LabelHeart labelText="XII passed" />,
    value: "XII passed",
  },
  {
    control_id: "f2",
    label: <LabelHeart labelText="Undergraduate" />,
    value: "Undergraduate",
  },
  {
    control_id: "f3",
    label: <LabelHeart labelText="Graduate" />,
    value: "Graduate",
  },
  {
    control_id: "f4",
    label: <LabelHeart labelText="Post Graduate" />,
    value: "Post Graduate",
  },
];

const questionlists = [
  {
    control_id: "f23",
    label: <LabelHeart labelText="Certificate" />,
    value: "Certificate",
  },
  {
    control_id: "f24",
    label: <LabelHeart labelText="Diploma" />,
    value: "Diploma",
  },
  {
    control_id: "f25",
    label: <LabelHeart labelText="Degree" />,
    value: "Degree",
  },
];

const questionlistss = [
  {
    control_id: "f35",
    label: <LabelHeart labelText="Yes" />,
    value: "Yes",
  },
  {
    control_id: "f36",
    label: <LabelHeart labelText="No" />,
    value: "No",
  },
];
const questionlistsalary = [
  {
    control_id: "f23",
    label: <LabelHeart labelText="1 Lakh & Above" />,
    value: "1 Lakh & Above",
  },
  {
    control_id: "f24",
    label: <LabelHeart labelText="2 Lakh & Above" />,
    value: "2 Lakh & Above",
  },
  {
    control_id: "f25",
    label: <LabelHeart labelText="3 Lakh & Above" />,
    value: "3 Lakh & Above",
  },
];
const questionlisty = [
  {
    id: 1,
    label: <LabelCheck labelText="Internships" />,
    value: "Internships",
  },
  {
    id: 2,
    label: <LabelCheck labelText="Junior (0-1 years)" />,
    value: "Junior (0-1 years)",
  },
  {
    id: 3,
    label: <LabelCheck labelText="Junior (1-2 years)" />,
    value: "Junior (1-2 years)",
  },
  {
    id: 4,
    label: <LabelCheck labelText="Mid-level (3-4 years)" />,
    value: "Mid-level (3-4 years)",
  },
];

const questionlistys = [
  {
    id: "q1",
    label: <LabelCheck labelText="Software Engineering" />,
    value: "Software Engineering",
  },
  {
    id: "q2",
    label: <LabelCheck labelText="Sales & Account Management" />,
    value: "Sales & Account Management",
  },
  {
    id: "q3",
    label: <LabelCheck labelText="Junior (1-2 years)" />,
    value: "Junior (1-2 years)",
  },
  {
    id: "q4",
    label: <LabelCheck labelText="Mid-level (3-4 years)" />,
    value: "Mid-level (3-4 years)",
  },
];

export const StepsForm = () => {
  const [stepsCount, setStepsCount] = useState(1);

  const schema1 = yup.object().shape({
    QualificationHighest: yup
      .string()
      .required("Please select your highest Qualification ?"),
  });

  const schema2 = yup.object().shape({
    Coursessuitsyou: yup.string().required("Please select suits you the best?"),
  });

  const schema3 = yup.object().shape({
    salaryExpectations: yup
      .string()
      .required("Please select your salary expectations?"),
  });

  const schema4 = yup.object().shape({
    levelOfRoles: yup
      .string()
      .required("Please select level of roles would you like to see?"),
  });
  const schema5 = yup.object().shape({
    specificCompany: yup
      .string()
      .required("Please select work for a specific size of specific Company?"),
  });

  const phoneval = /^[6789][0-9]{9}$/;
  const pinval = /^\d{6}$/;

  const schema6 = yup.object().shape({
    fullName: yup.string().required("Please enter your full name."),
    email: yup
      .string()
      .email("Please enter a valid email address.")
      .required("Please enter your email address."),
    dob: yup.string().required("Please enter your date of birth."),
    mobileNumber: yup
      .string()
      .matches(phoneval, "Please enter a valid 10-digit mobile number.")
      .required("Please enter your mobile number."),
    state: yup.string().required("Please select your state."),
    pincode: yup
      .string()
      .matches(pinval, "Please enter a valid 6-digit pin code number.")
      .required("Please enter your Pin code number."),
  });

  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(
      stepsCount === 1
        ? schema1
        : stepsCount === 2
        ? schema2
        : stepsCount === 3
        ? schema3
        : stepsCount === 4
        ? schema4
        : stepsCount === 5
        ? schema5
        : schema6
    ),
    defaultValues: {
      QualificationHighest: "",
      Coursessuitsyou: "",
      salaryExpectations: "",
      levelOfRoles: "",
      specificCompany: "",
      fullName: "",
      email: "",
      mobileNumber: "",
      dob: "",
      state: "",
    },
  });

  const router = useRouter();

  const finalSubmit = (data) => {
    if (stepsCount === 6) {
      axios
        .post(process.env.NEXT_PUBLIC_API_URL + "/stepsLeads", data)
        .then((resp) => {
          router.push("/thanku");
        })
        .catch((err) => {});
    } else {
      setStepsCount(stepsCount + 1);
    }
  };
  return (
    <section>
      <Container>
        <Row className="align-items-center">
          <>
            <Form onSubmit={handleSubmit(finalSubmit)}>
              {stepsCount === 1 ? (
                <>
                  <div className={`${styles.otta_mainHdrContent}`}>
                    <h4>
                      Hey, are you confused about your career? We have a
                      solution for you.{" "}
                    </h4>

                    <p className="mb-5">
                      Hey, are you being skeptical about your career? We have a
                      solution for you. Do you know that the BFSI sector will
                      become the third-largest sector by 2025, with a value of
                      around INR 81 trillion? Any organization, industry, or
                      business entity cannot operate without accounts and
                      finance professionals. Hence, there will be exponential
                      growth in job openings in the BFSI sector. Besides, Basik
                      Academy is always ready to assist you in choosing the
                      right career option in the field of accounts and finance
                      by providing industry-relevant skill sets through
                      experiential learning – with programs designed &amp;
                      delivered by industry experts.
                    </p>
                  </div>
                  <div className={`${styles.otta_header} text-center mx-auto`}>
                    <h4 className="fw-bold">
                      Can you tell us your highest Qualification so far?
                    </h4>
                  </div>
                  <div className={`${styles.option_wrapper} option_wrapper`}>
                    <div
                      className={`${styles.option_Content} d-flex flex-wrap col-md-5 mx-auto py-3 optionCont`}
                    >
                      {questionlist.map((list) => (
                        <Form.Group
                          key={generate()}
                          controlId={list.control_id}
                        >
                          <Form.Check
                            type="radio"
                            label={list.label}
                            value={list.value}
                            {...register("QualificationHighest")}
                            className="ottaRadio"
                          />
                        </Form.Group>
                      ))}
                    </div>
                    <p className="text-danger text-center mt-3">
                      {errors.QualificationHighest?.message}
                    </p>
                  </div>
                </>
              ) : stepsCount === 2 ? (
                <>
                  <div className={`${styles.otta_header} text-center mx-auto`}>
                    <h4 className="fw-bold">What suits you the best? </h4>
                  </div>
                  <div className={`${styles.option_wrapper} option_wrapper`}>
                    <div
                      className={`${styles.option_Content} d-flex flex-wrap col-md-5 mx-auto py-3 optionCont`}
                    >
                      {questionlists.map((list) => (
                        <Form.Group
                          key={generate()}
                          controlId={list.control_id}
                        >
                          <Form.Check
                            type="radio"
                            label={list.label}
                            value={list.value}
                            {...register("Coursessuitsyou")}
                            className="ottaRadio"
                          />
                        </Form.Group>
                      ))}
                    </div>
                    <p className="text-danger text-center mt-3">
                      {errors.Coursessuitsyou?.message}
                    </p>
                  </div>
                </>
              ) : stepsCount === 3 ? (
                <>
                  <div className={`${styles.otta_header} text-center mx-auto`}>
                    <h4 className="fw-bold">
                      Give us an idea about your salary expectations?
                    </h4>
                  </div>
                  <div className={`${styles.option_wrapper} option_wrapper`}>
                    <div
                      className={`${styles.option_Content} d-flex flex-wrap col-md-5 mx-auto py-3 optionCont`}
                    >
                      {questionlistsalary.map((list) => (
                        <Form.Group
                          key={generate()}
                          controlId={list.control_id}
                        >
                          <Form.Check
                            type="radio"
                            label={list.label}
                            value={list.value}
                            {...register("salaryExpectations")}
                            className="ottaRadio"
                          />
                        </Form.Group>
                      ))}
                    </div>
                    <p className="text-danger text-center mt-3">
                      {errors.salaryExpectations?.message}
                    </p>
                  </div>
                </>
              ) : stepsCount === 4 ? (
                <>
                  <div className={`${styles.otta_header} text-center mx-auto`}>
                    <h4 className="fw-bold">
                      What level of roles would you like to see?
                    </h4>
                  </div>
                  <div className={`${styles.option_wrapper} option_wrapper`}>
                    <div className="col-md-5 mx-auto py-3">
                      {questionlisty.map((list) => (
                        <Form.Group key={generate()} controlId={list.id}>
                          <Form.Check
                            type="radio"
                            label={list.label}
                            value={list.value}
                            {...register("levelOfRoles")}
                            className="ottaRadio"
                          />
                        </Form.Group>
                      ))}
                    </div>
                    <p className="text-danger text-center mt-3">
                      {errors.levelOfRoles?.message}
                    </p>
                  </div>
                </>
              ) : stepsCount === 5 ? (
                <>
                  <div className={`${styles.otta_header} text-center mx-auto`}>
                    <h4 className="fw-bold">
                      Do you want to work for a specific size of specific
                      Company?
                    </h4>
                  </div>
                  <div className={`${styles.option_wrapper} option_wrapper`}>
                    <div className="col-md-5 mx-auto py-3">
                      {questionlistys.map((list) => (
                        <Form.Group key={generate()} controlId={list.id}>
                          <Form.Check
                            type="radio"
                            label={list.label}
                            value={list.value}
                            {...register("specificCompany")}
                            className="ottaRadio"
                          />
                        </Form.Group>
                      ))}
                    </div>
                    <p className="text-danger text-center mt-3">
                      {errors.specificCompany?.message}
                    </p>
                  </div>
                </>
              ) : stepsCount === 6 ? (
                <>
                  <div className={`${styles.otta_header} text-center mx-auto`}>
                    <h4 className="fw-bold">More about you </h4>
                  </div>
                  <div
                    className={`${styles.option_wrapper} ${styles.login_wrapper} option_wrapper`}
                  >
                    <Row>
                      <Col md={5} className="mx-auto SubmitMainForm pb-3">
                        <div className="col-lg-12 mb-3">
                          <Form.Group controlId="fullName.ControlInput1">
                            <Form.Label className="myfomattribute p-0">
                              Full Name <span style={{ color: "red" }}>*</span>
                            </Form.Label>
                            <Form.Control
                              type="text"
                              placeholder="Enter your full name"
                              onChange={(e) =>
                                setValue("fullName", e.target.value, {
                                  shouldValidate: true,
                                })
                              }
                            />
                            <p
                              className="text-danger"
                              style={{ fontSize: "13px" }}
                            >
                              {errors.fullName?.message}
                            </p>
                          </Form.Group>
                        </div>
                        <div className="col-lg-12 mb-3">
                          <Form.Group controlId="email.ControlInput1">
                            <Form.Label className="myfomattribute p-0">
                              Email <span style={{ color: "red" }}>*</span>
                            </Form.Label>
                            <Form.Control
                              type="email"
                              placeholder="Enter your email address"
                              onChange={(e) =>
                                setValue("email", e.target.value, {
                                  shouldValidate: true,
                                })
                              }
                            />
                            <p
                              className="text-danger"
                              style={{ fontSize: "13px" }}
                            >
                              {errors.email?.message}
                            </p>
                          </Form.Group>
                        </div>
                        <div className="col-lg-12 mb-3">
                          <Form.Group controlId="dob.ControlInput1">
                            <Form.Label className="myfomattribute p-0">
                              Date of Birth{" "}
                              <span style={{ color: "red" }}>*</span>
                            </Form.Label>
                            <Form.Control
                              type="date"
                              placeholder="Enter your date of birth"
                              onChange={(e) =>
                                setValue("dob", e.target.value, {
                                  shouldValidate: true,
                                })
                              }
                            />
                            <p
                              className="text-danger"
                              style={{ fontSize: "13px" }}
                            >
                              {errors.dob?.message}
                            </p>
                          </Form.Group>
                        </div>
                        <div className="col-lg-12 mb-3">
                          <Form.Group controlId="mobile.ControlInput1">
                            <Form.Label className="myfomattribute p-0">
                              Mobile Number{" "}
                              <span style={{ color: "red" }}>*</span>
                            </Form.Label>
                            <Form.Control
                              type="tel"
                              placeholder="Enter your mobile number"
                              onChange={(e) =>
                                setValue("mobileNumber", e.target.value, {
                                  shouldValidate: true,
                                })
                              }
                            />
                            <p
                              className="text-danger"
                              style={{ fontSize: "13px" }}
                            >
                              {errors.mobileNumber?.message}
                            </p>
                          </Form.Group>
                        </div>
                        <div className="col-lg-12 mb-3">
                          <Form.Group controlId="state.ControlSelect1">
                            <Form.Label className="myfomattribute p-0">
                              Select State{" "}
                              <span style={{ color: "red" }}>*</span>
                            </Form.Label>
                            <Form.Control
                              as="select"
                              {...register("state")}
                              className="ottaRadio"
                            >
                              <option value="">Select state</option>
                              <option value="Andhra Pradesh">
                                Andhra Pradesh
                              </option>
                              <option value="Arunachal Pradesh">
                                Arunachal Pradesh
                              </option>
                              <option value="Assam">Assam</option>
                              <option value="Bihar">Bihar</option>
                              <option value="Chhattisgarh">Chhattisgarh</option>
                              <option value="Goa">Goa</option>
                              <option value="Gujarat">Gujarat</option>
                              <option value="Haryana">Haryana</option>
                              <option value="Himachal Pradesh">
                                Himachal Pradesh
                              </option>
                              <option value="Jharkhand">Jharkhand</option>
                              <option value="Karnataka">Karnataka</option>
                              <option value="Kerala">Kerala</option>
                              <option value="Madhya Pradesh">
                                Madhya Pradesh
                              </option>
                              <option value="Maharashtra">Maharashtra</option>
                              <option value="Manipur">Manipur</option>
                              <option value="Meghalaya">Meghalaya</option>
                              <option value="Mizoram">Mizoram</option>
                              <option value="Nagaland">Nagaland</option>
                              <option value="Odisha">Odisha</option>
                              <option value="Punjab">Punjab</option>
                              <option value="Rajasthan">Rajasthan</option>
                              <option value="Sikkim">Sikkim</option>
                              <option value="Tamil Nadu">Tamil Nadu</option>
                              <option value="Telangana">Telangana</option>
                              <option value="Tripura">Tripura</option>
                              <option value="Uttar Pradesh">
                                Uttar Pradesh
                              </option>
                              <option value="Uttarakhand">Uttarakhand</option>
                              <option value="West Bengal">West Bengal</option>
                              <option value="Andaman and Nicobar Islands">
                                Andaman and Nicobar Islands
                              </option>
                              <option value="Chandigarh">Chandigarh</option>
                              <option value="Dadra and Nagar Haveli and Daman and Diu">
                                Dadra and Nagar Haveli and Daman and Diu
                              </option>
                              <option value="Delhi">Delhi</option>
                              <option value="Lakshadweep">Lakshadweep</option>
                              <option value="Puducherry">Puducherry</option>
                            </Form.Control>
                            {errors.state && (
                              <p
                                className="text-danger"
                                style={{ fontSize: "13px" }}
                              >
                                {errors.state.message}
                              </p>
                            )}
                          </Form.Group>
                        </div>
                        <div className="col-lg-12 mb-3">
                          <Form.Group controlId="pin.ControlInput1">
                            <Form.Label className="myfomattribute p-0">
                              Pin Code <span style={{ color: "red" }}>*</span>
                            </Form.Label>
                            <Form.Control
                              type="tel"
                              placeholder="Enter your Pin Code"
                              onChange={(e) =>
                                setValue("pincode", e.target.value, {
                                  shouldValidate: true,
                                })
                              }
                            />
                            <p
                              className="text-danger"
                              style={{ fontSize: "13px" }}
                            >
                              {errors.pincode?.message}
                            </p>
                          </Form.Group>
                        </div>
                        <div className="col-lg-12 mb-3">
                          <Form.Group
                            className="mb-3"
                            controlId="exampleForm.ControlTextarea1"
                          >
                            <Form.Label className="myfomattribute p-0">
                              Message
                            </Form.Label>
                            <Form.Control
                              as="textarea"
                              rows={3}
                              placeholder="Type your message here"
                              {...register("message")}
                              className="ottaRadio"
                            />
                          </Form.Group>
                        </div>
                      </Col>
                    </Row>
                  </div>
                </>
              ) : (
                ""
              )}

              <div className="prvs_btn_nxt text-center my-3">
                <button
                  variant="primary"
                  type="button"
                  className={`backbutton btn ${
                    stepsCount === 1 || stepsCount === 6 ? "disabled" : ""
                  }`}
                  onClick={() => setStepsCount(stepsCount - 1)}
                  disabled={stepsCount === 1 || stepsCount === 6}
                >
                  Back
                </button>
                <button
                  variant="primary"
                  className="privkbutton btn"
                  type="submit"
                >
                  {" "}
                  {stepsCount === 6 ? "Submit" : "Next"}
                </button>
              </div>
            </Form>
          </>
        </Row>
      </Container>
    </section>
  );
};
