import React from "react";
import { Modal } from "react-bootstrap";
import WebinarCustomCard from "../webinar/WebinarCustomCard";
import VideoModalContent from "./VideoModalContent";

export const MasterVideo = () => {
  const [showModal, setShowModal] = React.useState(false);
  const [modalContent, setModalContent] = React.useState(null);

  function VideoModal() {
    return (
      <Modal
        size="lg"
        show={showModal}
        onHide={() => setShowModal(false)}
        centered
      >
        <Modal.Header closeButton />
        {modalContent}
      </Modal>
    );
  }

  function setupModal(key, eKey) {
    if (key === "video-modal") {
      setModalContent(<VideoModalContent videoSRC={eKey} />);
      setShowModal(true);
    }
  }

  return (
    <div className="container">
      <div className="row">
        <h1 className="heading-box">
          Sample of <span>Recording Session</span>
        </h1>

        <WebinarCustomCard
          CardImg="/course/indirectTax.png"
          Title01="Webinar"
          Title02="Ms. Kanika Tuli Sharma"
          Pagragraph="Guest Faculty at Delhi University"
          // DateWebinar="1 June 2022"

          onClick={() => setupModal("video-modal", "/video/GstVideoFor.mp4")}
        />
        <WebinarCustomCard
          CardImg="/course/course-image-5.png"
          Title01="Webinar"
          Title02="Mr. Atul Kumar"
          Pagragraph="Associate Professor & Research"
          // DateWebinar="1 June 2022"

          onClick={() => setupModal("video-modal", "/video/Excel-video.mp4")}
        />
        <WebinarCustomCard
          CardImg="/course/tally-prime-complete.png"
          Title01="Webinar"
          Title02="Mr. Atul Kumar"
          Pagragraph="Associate Professor & Research"
          // DateWebinar="1 June 2022"

          onClick={() => setupModal("video-modal", "/video/Tally-video.mp4")}
        />
        <WebinarCustomCard
          CardImg="/course/finance-accounting.png"
          Title01="Webinar"
          Title02="Ms. Kanchan Chabbra"
          Pagragraph="Senior Faculty at Delhi University"
          // DateWebinar="1 June 2022"

          onClick={() =>
            setupModal("video-modal", "/video/finance-accounting.mp4")
          }
        />
      </div>
      <VideoModal />
    </div>
  );
};

export default MasterVideo;
