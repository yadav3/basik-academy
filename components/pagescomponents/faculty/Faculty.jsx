import React from "react";
import Header from "../../globals/header/Header";
import Footer from "../../globals/footer/Footer";
import HeadingContent from "../../globals/headingContent/HeadingContent";
import StaticBanner from "../../globals/banner/StaticBanner";
import Card_Custom from "./Card_Custom";
import styles from "../../pagescomponents/faculty/Faculty.module.css";
import Image from "next/image";
import FacultySection from "./FacultySection";
import MasterVideo from "./MasterVideo";

export const Faculty = () => {
  return (
    <>
      <Header />

      <StaticBanner
        BannerImage="/banner/faculty/faculty-banner-img.png"
      />

      <section className={`${styles.facultySection} sectionGlobal`}>
        <div className="container">
          <div className="row">
            <div className="col-md-12 mb-4">
              <HeadingContent
                heading="Meet Our Faculty"
                pagragraph="Experiential learning is the key to transforming careers... Learn by Doing with the Industry Experts..."
              />
              College Vidya Skills Academy houses highly educated and well
              experienced council members and faculties from the field of
              banking, accounts & finance, they impart best of the knowledge,
              skills and training to the learners and make them industry
              relevant professionals from the beginning of their career journey
            </div>
            <FacultySection />
          </div>
          <div className="row">
            <div className="col-md-12 my-5">
              <HeadingContent
                heading="Learn from the <span>Masters</span>"
              />
            </div>
            <div className="col-lg-4 facultyCard01">
              <Card_Custom
                CardImage="/faculty/f1.png"
                CardTitle="Dr. Vivek Singh "
                CardText="Phd. Commerce 
                      <div>Training Consultant</div>"
                facultyName="Dr. Vivek Singh "
                facultyHistory="Dr. Vivek Singh is working with University of Delhi as Guest Faculty;
                       Associated with JMI as faculty; Subject Specialisation includes Retail Marketing &
                        Management Accounting "
              />
            </div>
            <div className="col-lg-4 facultyCard01">
              <Card_Custom
                CardImage="/faculty/f2.png"
                CardTitle="Amir "
                CardText="M.Phil-English
                      <div>Freelance Trainer</div>"
                facultyName="Amir "
                facultyHistory="Associate Professor & Research Assistant in Al.
                       Falah University & University of Western Australia. 
                       He has stint in translation & academic events compering. "
              />
            </div>
            <div className="col-lg-4 facultyCard01">
              <Card_Custom
                CardImage="/faculty/f3.png"
                CardTitle="Kanika Sharma  "
                CardText="M.Com,CA-Inter
                       <div>Trainer-Taxation</div>"
                facultyName="Kanika Sharma  "
                facultyHistory="CA Intermediate is specialised in Companies’ Financial Monitoring,
                        Return Filing, & GST Reconciliation; 3 years out of 10 years of her 
                        job experience in BFSI Sector pertains to Training. She is Certified Trainer from BFSI SSC. "
              />
            </div>


          </div>
        </div>
      </section>
      <section className="pt-1 pb-5 sectionGlobal">
        <MasterVideo />
      </section>
      <Footer />
    </>
  );
};

export default Faculty;
