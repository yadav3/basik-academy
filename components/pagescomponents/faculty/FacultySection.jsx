import React from "react";
import Card_Custom from "./Card_Custom";
export const FacultySection = () => {
  return (
    <>
      <div className="col-md-12">
        <h1 className="heading-box">
          Our <span>Academic Council</span>
        </h1>
        <p>The Academic Council - College Vidya Skills Academy has control over and is responsible for the maintenance of the standard of education, teaching and training, inter-department coordination, research, examinations, and tests within the academy.</p>
      </div>
      <div className="col-lg-4 facultyCard02 h-100">
        <Card_Custom
          CardImage="/faculty/c1.png"
          CardTitle="Preeti Gupta "
          CardText="M Com, MCA, MSW & M.ED 
                <div>Academic Head-Blackboard Group of Companies.</div>"
          facultyName="Preeti Gupta"
          facultyHistory="Erstwhile Assistant Director – Swami Vivekanand
          Subharti University, Assistant Prof. SIBM, Pune(yr.2007-09),
          Academic Council Member-Lingayas Vidyapeeth since yr.2019.
          She has stint in Design & Development of Curriculum & Content 
          and also has expertise on Students’ Career Counselling "
        />
      </div>
      <div className="col-lg-4 facultyCard02 h-100">
        <Card_Custom
          CardImage="/faculty/c2.png"
          CardTitle="Dr.Shahabuddin Usmani "
          CardText="PHD (Comm.)
         Academic Advisory & Consultancy (Prof. - Jamia Millia Islamia, University)"
          facultyName="Dr.Shahabuddin Usmani "
          facultyHistory="Dr. Usmani is Visiting Faculty in IGNOU (Dept. Of Commerce);
          Assistant Prof. in Al. Falah University (Dept. Of Management Studies); 
          Author of ‘Industrial Relations in India ‘published by Sonali 
          Publication in the Yr.2010 "
        />
      </div>
      <div className="col-lg-4 facultyCard02 h-100">
        <Card_Custom
          CardImage="/faculty/f4.png"
          CardTitle="Atul Kr. Agrawal "
          CardText="Master of Business Administration Finance
           Trainer-Computer & Accounts"
          facultyName="Atul Kr. Agrawal "
          facultyHistory="Associated with ICFE as Head-Academics; As R & D Head,
           he has been instrumental in development of Course Curriculum; has 
           imparted training on BFSI domain for almost 2.5 decades."
        />
      </div>
    </>
  );
};

export default FacultySection;
