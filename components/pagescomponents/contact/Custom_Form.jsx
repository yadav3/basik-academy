import React from "react";
import { Form, Button } from "react-bootstrap";
import styles from "../contact/Contact.module.css";
import CustomFormControl from "../../globals/CustomFormControl";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useRouter } from "next/router";
import axios from "axios";

const Custom_Form = () => {
  const phoneval = /^[6789][0-9]{9}$/;
  const pinval = /^\d{6}$/;

  const schema = yup.object().shape({
    fullName: yup.string().required("Please enter your full name."),
    email: yup
      .string()
      .email("Please enter a valid email address.")
      .required("Please enter your email address."),
    dob: yup.string().required("Please enter your date of birth."),
    mobileNumber: yup
      .string()
      .matches(phoneval, "Please enter a valid 10-digit mobile number.")
      .required("Please enter your mobile number."),
    state: yup.string().required("Please select your state."),
    pincode: yup
      .string()
      .matches(pinval, "Please enter a valid 6-digit pin code number.")
      .required("Please enter your Pin code number."),
    message: yup.string(),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const router = useRouter();

  const onSubmit = (data) => {
    axios
      .post(process.env.NEXT_PUBLIC_API_URL + "/leads", data)
      .then((resp) => {
        router.push("/thanku");
      })
      .catch((err) => {});
  };

  return (
    <Form className="row" onSubmit={handleSubmit(onSubmit)}>
      <div className="col-lg-12 mb-3">
        <Form.Group controlId="fullName.ControlInput1">
          <Form.Label>
            Full Name <span style={{ color: "red" }}>*</span>
          </Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter your full name"
            {...register("fullName")}
          />
          {errors.fullName && (
            <p className="text-danger" style={{ fontSize: "13px" }}>
              {errors.fullName.message}
            </p>
          )}
        </Form.Group>
      </div>
      <div className="col-lg-12 mb-3">
        <Form.Group controlId="email.ControlInput1">
          <Form.Label>
            Email <span style={{ color: "red" }}>*</span>
          </Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter your email address"
            {...register("email")}
          />
          {errors.email && (
            <p className="text-danger" style={{ fontSize: "13px" }}>
              {errors.email.message}
            </p>
          )}
        </Form.Group>
      </div>
      <div className="col-lg-12 mb-3">
        <Form.Group controlId="dob.ControlInput1">
          <Form.Label>
            Date of Birth <span style={{ color: "red" }}>*</span>
          </Form.Label>
          <Form.Control
            type="date"
            placeholder="Enter your date of birth"
            {...register("dob")}
          />
          {errors.dob && (
            <p className="text-danger" style={{ fontSize: "13px" }}>
              {errors.dob.message}
            </p>
          )}
        </Form.Group>
      </div>
      <div className="col-lg-12 mb-3">
        <Form.Group controlId="mobile.ControlInput1">
          <Form.Label>
            Mobile Number <span style={{ color: "red" }}>*</span>
          </Form.Label>
          <Form.Control
            type="tel"
            placeholder="Enter your mobile number"
            {...register("mobileNumber")}
          />
          {errors.mobileNumber && (
            <p className="text-danger" style={{ fontSize: "13px" }}>
              {errors.mobileNumber.message}
            </p>
          )}
        </Form.Group>
      </div>
      <div className="col-lg-12 mb-3">
        <Form.Group controlId="state.ControlSelect1">
          <Form.Label>
            Select State <span style={{ color: "red" }}>*</span>
          </Form.Label>
          <Form.Control as="select" {...register("state")}>
            <option value="">Select state</option>
            <option value="Andhra Pradesh">Andhra Pradesh</option>
            <option value="Arunachal Pradesh">Arunachal Pradesh</option>
            <option value="Assam">Assam</option>
            <option value="Bihar">Bihar</option>
            <option value="Chhattisgarh">Chhattisgarh</option>
            <option value="Goa">Goa</option>
            <option value="Gujarat">Gujarat</option>
            <option value="Haryana">Haryana</option>
            <option value="Himachal Pradesh">Himachal Pradesh</option>
            <option value="Jharkhand">Jharkhand</option>
            <option value="Karnataka">Karnataka</option>
            <option value="Kerala">Kerala</option>
            <option value="Madhya Pradesh">Madhya Pradesh</option>
            <option value="Maharashtra">Maharashtra</option>
            <option value="Manipur">Manipur</option>
            <option value="Meghalaya">Meghalaya</option>
            <option value="Mizoram">Mizoram</option>
            <option value="Nagaland">Nagaland</option>
            <option value="Odisha">Odisha</option>
            <option value="Punjab">Punjab</option>
            <option value="Rajasthan">Rajasthan</option>
            <option value="Sikkim">Sikkim</option>
            <option value="Tamil Nadu">Tamil Nadu</option>
            <option value="Telangana">Telangana</option>
            <option value="Tripura">Tripura</option>
            <option value="Uttar Pradesh">Uttar Pradesh</option>
            <option value="Uttarakhand">Uttarakhand</option>
            <option value="West Bengal">West Bengal</option>
            <option value="Andaman and Nicobar Islands">
              Andaman and Nicobar Islands
            </option>
            <option value="Chandigarh">Chandigarh</option>
            <option value="Dadra and Nagar Haveli and Daman and Diu">
              Dadra and Nagar Haveli and Daman and Diu
            </option>
            <option value="Delhi">Delhi</option>
            <option value="Lakshadweep">Lakshadweep</option>
            <option value="Puducherry">Puducherry</option>
          </Form.Control>
          {errors.state && (
            <p className="text-danger" style={{ fontSize: "13px" }}>
              {errors.state.message}
            </p>
          )}
        </Form.Group>
      </div>
      <div className="col-lg-12 mb-3">
        <Form.Group controlId="mobile.ControlInput1">
          <Form.Label>
            Pin Code<span style={{ color: "red" }}>*</span>
          </Form.Label>
          <Form.Control
            type="tel"
            placeholder="Enter your pin Code"
            {...register("pincode")}
          />
          {errors.pincode && (
            <p className="text-danger" style={{ fontSize: "13px" }}>
              {errors.pincode.message}
            </p>
          )}
        </Form.Group>
      </div>
      <div className="col-lg-12 mb-3">
        <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
          <Form.Label>Message</Form.Label>
          <Form.Control
            as="textarea"
            rows={3}
            placeholder="Type your message here"
            {...register("message")}
          />
        </Form.Group>
      </div>
      <div className="col-lg-12">
        <Button
          variant="primary"
          type="submit"
          className={`${styles.contactBtn}`}
        >
          Send Message
        </Button>
      </div>
    </Form>
  );
};

export default Custom_Form;
