import React from "react";
import Header from "../../globals/header/Header";
import Footer from "../../globals/footer/Footer";
import StaticBanner from "../../globals/banner/StaticBanner";
import HeadingContent from "../../globals/headingContent/HeadingContent";
import styles from "../placement/Placement.module.css";
import Image from "next/image";
import { Form, Button, Row, Container, Col } from "react-bootstrap";
import PartnerCounter from "../../globals/partnerCounter/PartnerCounter";
import CustomFormControl from "../../globals/CustomFormControl";
import Link from "next/link";
import { Pointers } from "../../globals/mainPointers/Pointers";
import FormModal from "../../globals/modals/FormModal";
import { useEffect, useState } from "react";

export const Conssult = () => {
  const [showModal, setShowModal] = useState(false);

  const handleShowModal = () => {
    setShowModal(true);
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  useEffect(() => {}, [showModal]);

  return (
    <>
      <Header />
      <StaticBanner BannerImage="/banner/placement/consultant.jpg" />
      <section className={`${styles.ConsSection01} sectionGlobal`}>
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <HeadingContent
                heading="Employers & <span>Consultants</span>"
                pagragraph="<p>College Vidya Skills Academy has dedicated placement cell 
                to function throughout the year for generating suitable 
                employment opportunities for our trained candidates in the industry.
                 Our Placement team connects with the industry recruitment heads, potential partners to  
                 understand their manpower needs and match the profile
                  of our eligible candidates and place them at right place. 
                  If you are an employer looking for trained manpower for your organization you are at right place 
        
                </p>
                  "
              />
            </div>
          </div>
        </div>
      </section>

      <section className="ContactUsNow">
        <Container fluid>
          <Row className="align-items-center">
            <Col className="text-center">
              <h2 className="mb-5">
                We are here to train you with our expert team of Industry. Leave
                your details.
              </h2>
              <a
                className="talkExBtn mycontctbtn anchorbtnStyle"
                onClick={handleShowModal}
              >
                Talk to Experts
              </a>
            </Col>
          </Row>
        </Container>
        <div className="talkSection">
          <FormModal show={showModal} handleCloseModal={handleCloseModal} />
        </div>
      </section>

      <Pointers />

      <PartnerCounter />

      <Footer />
    </>
  );
};

export default Conssult;
