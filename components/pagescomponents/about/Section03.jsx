import React from "react";
import { section } from "react-bootstrap";
import styles from "../about/About.module.css";
import Image from "next/image";

export const section03 = (props) => {
  return (
    <>
      <section
        className={`${styles.aboutsection03} aboutsecton03 sectionGlobal`}
      >
        <div className={`${styles.borderTopBtm} container`}>
          <div className="row">
            <div className="col-md-5">
              <div className={styles.lftBox}>
                <div className={styles.btmBox}>
                  <Image
                    src="/about/CEO.jpg"
                    width={550}
                    height={800}
                    alt="CEO"
                  />
                </div>
              </div>
            </div>
            <div className="col-md-7 mb-3">
              <div className={styles.rgtBox}>
                <h1>
                  Message from{" "}
                  <span className="blueColor">CEO&apos;s Desk </span>{" "}
                </h1>
                <p style={{ textAlign: "justify", lineHeight: "32px" }}>
                  Thank you for your interest in CV Skills Academy. We have been
                  providing high-quality education in the field of Accounts,
                  Finance, and Taxation for the past two decades to students who
                  have completed their 10+2 or undergraduate education. We are
                  proud to have certified over 10,000 learners and placed over
                  8,000 candidates in the Banking, Financial Services, and
                  Insurance Industry (BFSI) since the inception of our training
                  and education programs.
                </p>
                <p style={{ textAlign: "justify", lineHeight: "32px" }}>
                  The BFSI industry in India is expected to grow rapidly due to
                  factors such as increasing per capita income, innovation in
                  technology, expanding distribution networks, and rising
                  customer awareness of financial products. According to the
                  National Skill Development Corporation (NSDC), the BFSI sector
                  in India will require an additional 1.6 million skilled
                  workers by 2025.
                </p>
                <p style={{ textAlign: "justify", lineHeight: "32px" }}>
                  At CV Skills Academy, we focus on bridging the skills gap
                  required by various industries. Our courses are designed to
                  provide learners with on-the-job training in an informal
                  manner, preparing them for successful careers in their chosen
                  fields.
                </p>
                <p style={{ textAlign: "justify", lineHeight: "32px" }}>
                  We welcome you to explore the various academic programs we
                  offer and encourage you to contact us for additional
                  information. We are committed to helping you on your journey
                  to a successful career.
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default section03;
