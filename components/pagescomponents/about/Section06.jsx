import React from "react";
import Image from "next/image";
import styles from "../about/About.module.css";
import { Col, Container, Row } from "react-bootstrap";

export const Section06 = () => {
  return (
    <>
      <section className={`${styles.section06} sectionGlobal`}>
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <h1>
                Our<span className="blueColor"> Core Values</span>
              </h1>
              <ul>
                <li>
                  <Image
                    src="/about/c8.png"
                    width="151"
                    height="140"
                    alt="value"
                  />
                  <div className={`${styles.contBox}`}>
                    <h4>Trust</h4>
                    <p>
                      CV Skills Academy emerged from a company with 20+ years of experience in the education industry.
                    </p>
                  </div>
                </li>
                <li>
                  <Image
                    src="/about/c7.png"
                    width="151"
                    height="140"
                    alt="value"
                  />
                  <div className={`${styles.contBox}`}>
                    <h4>Optimism</h4>
                    <p>
                      CV Skills Academy’s value proposition is based on the emotion of optimism among students that they can get sustainable jobs.
                    </p>
                  </div>
                </li>
                <li>
                  <Image
                    src="/about/c11.png"
                    width="151"
                    height="140"
                    alt="value"
                  />
                  <div className={`${styles.contBox}`}>
                    <h4>Future-Focused</h4>
                    <p>
                      CV Skills Academy aims towards preparing students not just for contemporary industry needs but also enables them to be future-ready professionals.
                    </p>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </section>
      <section className="my-5 p-4">
        <Container>
          <h1 className="text-center">Our <span className="text-primary">Journey</span> </h1>
          <Row>
            <Col md={3}>
              <div className="shadow rounded bg-light p-4 h-100 mt-3">
                <h3 className="text-primary"><strong>1996</strong></h3>
                <p>We began our professional journey with IT education in 1996. And introduced a computer training institute for various skill development programs.</p>
              </div>
            </Col>
            <Col md={3}>
              <div className="shadow rounded bg-light p-4 h-100 mt-3">
                <h3 className="text-primary"><strong>2003</strong></h3>
                <p>ICFE was formed in the year of 2003 with the intent of imparting vocational training in computerized accounts and finance. </p>
              </div>
            </Col>
            <Col md={3}>
              <div className="shadow rounded bg-light p-4 h-100 mt-3">
                <h3 className="text-primary"><strong>2010</strong></h3>
                <p>In 2010, we ventured into the higher education domain with a core objective to provide comprehensive career guidance solutions to students, job seekers, and working professionals.</p>
              </div>
            </Col>
            <Col md={3}>
              <div className="shadow rounded bg-light p-4 h-100 mt-3">
                <h3 className="text-primary"><strong>2019</strong></h3>
                <p>In the year 2019, the College Vidya trademark was formulated which is now India’s leading online neutral and unbiased education platform connecting prospective learners to the right choice of University.</p>
              </div>
            </Col>
          </Row>
        </Container>
      </section>
    </>
  );
};

export default Section06;
