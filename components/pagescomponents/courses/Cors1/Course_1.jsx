import React, { useState, useEffect } from "react";
import Header from "../../../globals/header/Header";
import CustomBanner from "../courseBanner/CustomBanner";
import HeadingContent from "../../../globals/headingContent/HeadingContent";
import KeyHighLight from "../KeyHighLight";
import GetCourse from "../GetCourseSection/GetCourse";
import TalkComponentGlobal from "../../../globals/talkExpertLink/TalkComponentGlobal";
import Footer from "../../../globals/footer/Footer";
import styles from "../Course.module.css";
import Image from "next/image";
import CourseContent from "../CourseContent";
import MasterVideo from "../../faculty/MasterVideo";
import Certificate from "../../../globals/certificate/Certificate";
import BasikCustomTab from "../../../globals/basiksPride/BasikCustomTab";
import Fees from "../../../globals/fees/Fees";
import { Container } from "react-bootstrap";
import { Tab, Row, Col, Nav } from "react-bootstrap";
import { Pointers } from "../../../globals/mainPointers/Pointers";
import ProductsCard from "../../../globals/ourCourses/ProductsCard";
import Section07 from "../../about/Section07";
import Table from 'react-bootstrap/Table';

export const Course_1 = () => {
  const contents = [
    {
      header: "Module 1: Computer Application for Business-2",
      body: "MS Windows 10, MS-Word 2019, MS-Excel 2019, MS-PowerPoint 2019, Internet & E-Mail",
    },
    {
      header: "Module 2: Financial Accounting-2",
      body: "Module 2 Body",
    },
    {
      header: "Module 3: Tally Prime",
      body: "Module 3 Body",
    },
    {
      header: "Module 4: Indirect Tax -2 Goods & Service Tax (GST)",
      body: "Module 3 Body",
    },
    {
      header: "Module 5: Direct Tax-2",
      body: "Module 3 Body",
    },
    {
      header: "Module 6: Banking Law & Practices ",
      body: "Module 3 Body",
    },
    {
      header: "Module 7: English Communications & Personality Development",
      body: "Module 3 Body",
    },
  ];
  const [show, setShow] = useState(false);
  const controlNavbar = () => {
    if (window.scrollY > 300) {
      setShow(true);
    } else {
      setShow(false);
    }
  };

  useEffect(() => {
    window.addEventListener("scroll", controlNavbar);
    return () => {
      window.removeEventListener("scroll", controlNavbar);
    };
  }, []);

  const [basicActive, setBasicActive] = useState("tab1");
  const handleBasicClick = (id) => {
    const element = document.getElementById(id);
    if (element === basicActive) {
      return;
    }

    setBasicActive(element);

    window.scrollTo({
      behavior: "smooth",
      top: element.offsetTop - 80,
    });
  };

  return (
    <>
      <Header />
      <CustomBanner
        banerImage="/banner/courses/3-years-course-banner-img.jpg"
        title01="Work Integrated Degree
        Program"
        title02="BBA in Bussiness Accounting and Finance "
        title03="3 Years : Online"
        imgTitle="/course/staricon.png"
        title04="7686 Ratings"
        title05="4500+ Learners"
        title06="A first-ever course that integrates academic learning with its workplace application"
        VideoPath="/video/DirectTax.mp4"
      />
      <section>
        <header
          className={`${styles.stickyMenuContainer} ${show && "SticyMenuActive"
            } hidden`}
        >
          <div variant="pills" className={`${styles.menuName} me-auto`}>
            <div
              className={`${styles.nvNme}`}
              onClick={() => handleBasicClick("tab1")}
              active={basicActive === "tab1"}
            >
              Overview
            </div>
            <div
              className={`${styles.nvNme}`}
              onClick={() => handleBasicClick("tab2")}
              active={basicActive === "tab2"}
            >
              Benefits
            </div>
            <div
              className={`${styles.nvNme}`}
              onClick={() => handleBasicClick("tab4")}
              active={basicActive === "tab4"}
            >
              Sample
            </div>
            <div
              className={`${styles.nvNme}`}
              onClick={() => handleBasicClick("tab5")}
              active={basicActive === "tab5"}
            >
              Placement &amp; Reviews
            </div>
            <div
              className={`${styles.nvNme}`}
              onClick={() => handleBasicClick("tab6")}
              active={basicActive === "tab6"}
            >
              Talk to Expert
            </div>
            {/* <div
              className={`${styles.nvNme}`}
              onClick={() => handleBasicClick("tab5")}
              active={basicActive === "tab5"}
            >
              Certificate
            </div> */}

            {/* <div
              className={`${styles.nvNme}`}
              onClick={() => handleBasicClick("tab7")}
              active={basicActive === "tab7"}
            >
              Fees
            </div> */}
          </div>
        </header>
        <div className="courseStickyMenu">
          <div id="tab1" show={basicActive === "tab1"}>
            <section className={`${styles.sectionContent01} sectionGlobal`}>
              <div className="container">
                <div className="row">
                  <div className="col-md-7">
                    <div className={`${styles.box01}`}>
                      <HeadingContent
                        heading="About the BBA in Bussiness <span>Accounting and Finance</span>"
                        pagragraph="<p>BBA Business Accounting & Finance Studies is a 3-year course that provides students with the adequate skills required for a specific trade. This undergraduate course mainly focuses on Skill-Based Education. </p>
                        <p><strong> BBA:</strong> Features of Business Accounting & Finance</p>
                        <p> Students are awarded a Diploma after the completion of 1st Year.</p>
                        <p>The student is awarded an Advanced Diploma after completion of 2nd Year.</p>
                        <p>A Degree is awarded post-successful completion of the 3 years program. </p>
                        <p>Student gets eligible for a Job as an Entry-Level Accountant after successful completion of 1st Year.</p>
                        <p>The student also becomes ready to start his/her own business in the field of Accounts & Finance.</p>
                        <ul>
                        <li>⦁	Diploma (1 Year) </li>
                        <li>⦁	Advanced Diploma (2 Years)</li>
                        <li>⦁	BBA Degree (3 Years)</li>
                        </ul>
                        
                        
                        "

                      />
                    </div>
                  </div>
                  <div className="col-md-5">
                    <KeyHighLight />
                  </div>
                </div>
              </div>
            </section>
            <section className="p-5 my-3" style={{ background: "#194AEA" }}>
              <div className="container">
                <div className="row">
                  <h1 className="text-light">CV SKILLS ADVANTAGE</h1>
                  <hr />
                  <p className="text-justify text-light">and the current academic course curriculum. To address this gap, we have dedicated ourselves to bridging it in a seamless manner. Our focus is to help students acquire industry-relevant skill sets by offering experiential learning opportunities. Our programs are designed and delivered by industry experts, ensuring that students receive practical knowledge and skills that align with the needs of the job market.</p>
                  <div className="col-md-4 mt-3">
                    <div className="h-100 rounded-3 border shadow p-4" style={{ background: "#fff" }}>
                      <ul>
                        <li><b>1. Experienced Faculties</b></li>
                        <li><b>2. ‘e’ Taxation Expert</b></li>
                        <li><b>3. Seminars &amp; Webinar</b></li>
                        <li><b>4. Industry Base Projects Live Projects</b></li>
                      </ul>
                    </div>
                  </div>
                  <div className="col-md-4 mt-3">
                    <div className="h-100 rounded-3 border shadow p-4" style={{ background: "#fff" }}>
                      <ul>
                        <li><b>5. In-house “E-Mitra”-LMS ERP Support</b></li>
                        <li><b>6. Simulation Software</b></li>
                        <li><b>7. Pre-Recorded Class Session</b></li>
                        <li><b>8. Pro-AJE Accounting Perfection</b></li>
                      </ul>
                    </div>
                  </div>
                  <div className="col-md-4 mt-3">
                    <div className="h-100 rounded-3 border shadow p-4" style={{ background: "#fff" }}>
                      <ul>
                        <li><b>9. In-house R &amp; D</b></li>
                        <li><b>10. Direct Connect to Faculty</b></li>
                        <li><b>11. PDP Classes</b></li>
                        <li><b>12. Pre-Recorded Assig. Solution</b></li>
                      </ul>
                    </div>
                  </div>

                </div>
              </div>
            </section>
            <section className="mt-5 p-4" style={{ background: "#f5f5f5" }}>
              <div className="container">
                <div className="row align-items-center">
                  <div className="col-md-12">
                    <h1>E-MITRA: An LMS that prioritizes the engagements</h1>
                    <hr />
                    <p>LMS stands for Learning Management System. LMS enables a better Learning Environment. Helps us to deliver adequate & updated educational content, as well as monitor student progress and performance. It also facilitates communication between teachers and students, provides online forums, and allows for the exchange of information and resources content for E-mitra page</p>
                  </div>
                  <div className="col-md-6 mt-4">
                    <h1>Pro-AJE:</h1>
                    <p>Proficiency- in Accounting Journal Entries is a Value-Added Offering for Learners. The Learner gets to practice above 2,000 Journal Entries to practice the routine & get a good holding with the following benefits:</p>
                    <p>Pro-AJE has been developed for the learners to get the On-Job Training Experience. A Learner is given an Opening Balance Sheet & Closing Balance Sheet of a company with various nature of accounting entries taking place around the year to practice himself.</p>
                    <p>These Pro-AJE build up the confidence of the Learners. During their initial days of the JOB, the Learner quickly adjusts himself to any nature of business the employer is into. Win-Win Situations for both the Employer & the Learner.</p>
                    <ul>
                      <li>1. Chronological order of occurrence.</li>
                      <li>2. Decreases chances of errors </li>
                      <li>3. Provides information about each transaction </li>
                      <li>4. Reduces spending efforts & time</li>
                    </ul>
                  </div>
                  <div className="col-md-6 mt-4">
                    <Image src='/course/lmsimg.png' height={500} width={600} alt="lmsimg" className="mainlms" />
                  </div>
                </div>
              </div>
            </section>
          </div>
          <div id="tab2" show={basicActive === "tab2"}>
            <Pointers />
          </div>
          <Section07 />
          <div id="tab4" show={basicActive === "tab4"}>
            <section className={`${styles.masterVideo} masterVideo`}>
              <MasterVideo />
            </section>
          </div>
          {/* <div id="tab5" show={basicActive === "tab5"}>
            <section className={`${styles.CertificateBox}`}>
              <Certificate />
            </section>
          </div> */}
          <div id="tab5" show={basicActive === "tab5"}>
            <section className="basikCont sectionGlobal">
              <div className="container">
                <div className="row">
                  <div className="col-md-12">
                    <BasikCustomTab />
                  </div>
                </div>
              </div>
            </section>
          </div>
          {/* <div id="tab7" show={basicActive === "tab6"}>
            <section className="sectionGlobal">
              <div className="container">
                <div className="row">
                  <div className="col-md-12">
                    <Fees />
                  </div>
                </div>
              </div>
            </section>
          </div> */}
          <div id="tab6" show={basicActive === "tab6"}>
            <section className="aboutEnrollSection">
              <TalkComponentGlobal
                Heading="Want to see yourself Job-Ready? "
                buttonName="Enroll Now"
              />
            </section>
          </div>
        </div>
      </section>


      <Footer />
    </>
  );
};
export default Course_1;
