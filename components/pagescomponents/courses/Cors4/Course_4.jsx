import React, { useState, useEffect } from "react";
import Header from "../../../globals/header/Header";
import CourseBanner from "../courseBanner/CustomBanner";
import HeadingContent from "../../../globals/headingContent/HeadingContent";
import KeyHighLight from "../KeyHighLight";
import GetCourse from "../GetCourseSection/GetCourse";
import ButtonContainer from "../acordianSection/ButtonContainer";
import CustomAccordian from "../acordianSection/CustomAccordian";
import TalkComponentGlobal from "../../../globals/talkExpertLink/TalkComponentGlobal";
import Footer from "../../../globals/footer/Footer";
import styles from "../Course.module.css";
import CourseContent from "../CourseContent";
import MasterVideo from "../../faculty/MasterVideo";
import Certificate from "../../../globals/certificate/Certificate";
import BasikCustomTab from "../../../globals/basiksPride/BasikCustomTab";
import Fees from "../../../globals/fees/Fees";
import { Container } from "react-bootstrap";
import { Pointers } from "../../../globals/mainPointers/Pointers";
import Section07 from "../../about/Section07";
import Image from "next/image";

export const Course_4 = () => {
  const contents = [
    {
      header: "Computer Applications - (Duration- 02 months)",
      body: `
      <p><b>Microsoft Office 2019</b></p> 
      <div><b>MS-Word 2019</b></div>
      <p>QAT & Its Options, File Setup, Creation & Formatting, 
      Working with Tabs & Columns, TOC, Hyperlink, Bookmark, 
      Tracking, Table, Invoice Format with Draw Table, Formulas 
      in Table, Mail Merge, File Protection, Macros, Exporting 
      Docs in PDF & Other Format, Converting PDF to Doc, 
      Printing of document.<br><b class="color1">Practical Assignment</b></p>

      <div><b>MS-Excel 2019</b></div>
      <p>ntro, Working with Row, Column, Worksheet, Formatting 
      Row, Column, Worksheet, Name Manager, Formula Library, 
      Different Types of Chart Formation, Auditing, Math 
      Functions, Statistical Functions, Text Functions, Date & 
      Time Functions, Logical Functions, Lookup Functions 
      (Vlookup / Hlookup), Consolidate, Group / Ungroup, Data 
      Table, Goal Seek, Scenario, Sort & Filter, File Protection, 
      Printing of Row Headings on Each Page, Printing of 
      Documents.<br><b class="color1">Practical Assignment</b></p>     
      <div><b>MS-PowerPoint 2019</b></div>
      <p>Creation of Presentation, UsingTemplate, Inserting 
      /Deleting /Moving Slides in a Presentation, Duplicate a Slide, 
      Inserting Word Table / Excel Worksheet / Chart, Adding Clip 
      Art Pictures, Working with Colour, Creating Hyperlink Slide, Slides Setup, Adding Audio / Video, Setting of 
      Animation, Presentation of Slides, Transition, Slide Timings
      and Printing of Slides.<br> <b class="color1">Practical Assignment</b></p>

      <div><b>Internet & E-Mail</b></div>
      <p>Introduction of Internet & E-mail, Local Area Network (LAN), 
      Wide Area Network (WAN), World Wide Web & Websites, Web 
      Browsing Software, Search Engines, Downloading from 
      Websites, Basics of E-Mail, Email Account Creation, 
      Composing, Sending, Receiving E-mail, Sending Softcopy as 
      Attachment, Forwarding of Mail, Deleting Mail, Handling 
      Trash, Spam and Printing of E-mail.<br><b class="color1">Practical Assignment</b></p>
      `,
    },
    {
      header: "Certificate In Tally Prime Expert - (Duration- 02 months)",
      body: `      
      <div><b>Tally Prime- VOL-1 (Accounts) </b></div>
      <p>History of Tally, Company Creation & Alteration,Voucher
      Creation & Alteration, Deletion, Primary Group, Secondary 
      Group, Creation of Secondary Group, Alteration of Group, 
      Voucher Transactions, Report Generation Day Book, 
      Ledgers, Bank Book, Trial Balance, Trading & Profit & Loss 
      A/c, Day Book Functions, Budget and Control, Cost Category 
      and Cost Center, Security Maintain different Types, Banking 
      (Cheque Entry) Reconciliation, Backup of Data, Restore of 
      Data, Exporting Different Reports in Excel Format, PDF 
      Format, Printing of Tally Accounting Reports i.e. Day Book, 
      Vouchers, Ledgers, Cash Book, Bank Book, Trial Balance, 
      Trading A/c, Profit & Loss, Balance Sheet.<br><b class="color1">Practical Assignment</b></p>
      `,
    },
    {
      header: "Ms-Excel With Advance Functions & MIS - (Duration- 02 months)",
      body: `
      <p><b>Microsoft Office 2019</b></p> 
      <div><b>MS-Excel 2019 </b></div>
      <p>Intro, Working with Row, Column, Worksheet, Formatting 
      Row, Column, Worksheet, Name Manager, Formula Library, 
      Different Types of Chart Formation, Auditing, Math 
      Functions, Statistical Functions, Text Functions, Date & 
      Time Functions, Logical Functions, Lookup Functions 
      (Vlookup / Hlookup), Consolidate, Group / Ungroup, Data 
      Table, Goal Seek, Scenario, Sort & Filter, File Protection, 
      Printing of Row Headings on Each Page, Printing of 
      Documents.<br><b class="color1">Practical Assignment</b></p>
      
      <div><b>Tally Prime- VOL-2 (Accounts & Inventory)</b></div>
      <p>Company Creation with Accounts & Inventory Features, 
      Voucher Creation, Alteration & Deletion, Day Book 
      Functions, Process of Maintaining Inventory, Maintaining 
      Multiple Godowns, Actual & Bill QTY, Batch-wise Stock 
      Maintain, Transfer of Material, Order Processing, Multiple 
      Price Level, Currency Creation (Export – Country), Physical 
      Voucher, Manufacture Voucher, Bill of Material, Job Costing, 
      Interest Calculation, POS, Backup Restore, Exporting Tally 
      Inventory Reports in Different Formats (Excel, PDF, JPEG), 
      Printing of Tally Inventory Reports - Day Book, Invoice, 
      Godown Report, Party Ledgers, Trial Balance, Profit & Loss, 
      Balance Sheet.<br><b class="color1">Practical Assignment</b></p>
      
      <div><b>Advance Excel with MIS</b></div>
      <p>Data Protection, Macro (Without VBA), Group/Ungroup, 
      Conditional Formatting, Financial Functions – PV, FV, PMT, 
      PPMT, IPMT, DB, SLN, SYD, CUMPRINC, Database Functions, 
      Statistical Functions, Maths Functions, Advance Date & 
      Time Functions, Array Formula, Advance Vlookup & Hlookup, 
      Data Table, Working with Form, Flash Fill, Advance Filter, 
      Data Validation, Subtotal, Pivot Table & Chart – MIS Report 
      and Excel Templates.<br><b class="color1">Practical Assignment</b></p>

      <div><b>Tally Prime- VOL-3 (Taxation & Payroll)</b></div>
      <p>Tally Statutory & Taxation, Activation of GST, GST Ledgers, 
      CGST, SGST, IGST, Item Creation GST with HSN, Purchase 
      with GST, Sales with GST, Purchase Return, Sales Return, 
      E-Invoicing, GST Calculation, Input & Output Adjustment, 
      Input Tax Credit, Stock Calculation, GST Payable / 
      Refundable Adjustment Entry, GST Return - GST-1, GSTR 
      2A/2B GSTR 3B, GST Challan, Capital Goods in GST, Export in 
      GST, RCM Entry, E Way Bill, Challan Reconciliation, GSTR9, 
      GSTR9A/9C, TDS & TCS in Tally Prime, TDS Report, Form 
      24Q, 26Q, 27Q, Form 16, 16A, 26AS Reconciliation, Payroll in 
      Tally Prime, Creation of Unit, Attendance Creation, Creation 
      of Employee, Creation of Pay heads, Deduction, PF, ESI, 
      Calculation on Attendance & Production, Statement of 
      Payroll, Payslip, Gratuity Report, PF/ESI Report, Tally Shop, 
      Tally TDL, Data Configuration, Tally.Net Server, Tally Rental
      and Tally Audit.<br><b class="color1">Practical Assignment</b></p>
      

      
      `,
    },
  ];

  const [show, setShow] = useState(false);
  const controlNavbar = () => {
    if (window.scrollY > 300) {
      setShow(true);
    } else {
      setShow(false);
    }
  };

  useEffect(() => {
    window.addEventListener("scroll", controlNavbar);
    return () => {
      window.removeEventListener("scroll", controlNavbar);
    };
  }, []);

  const [basicActive, setBasicActive] = useState("tab1");
  const handleBasicClick = (id) => {
    const element = document.getElementById(id);
    if (element === basicActive) {
      return;
    }

    setBasicActive(element);

    window.scrollTo({
      behavior: "smooth",
      top: element.offsetTop - 80,
    });
  };
  return (
    <>
      <Header />

      <CourseBanner
        banerImage="/banner/courses/short-term-course-banner.jpg"
        title01="Skill Enhancement Program"
        title02="Accounting Software Applications"
        title03="2.5 Months: Online"
        imgTitle="/course/staricon.png"
        title04="6590 Ratings"
        title05="3900+ Learners"
        title06="Focused on tracking a vocation on a specialized subject"
        VideoPath="/video/GstVideoFor.mp4"
      />
      <header
        className={`${styles.stickyMenuContainer} ${
          show && "SticyMenuActive"
        } hidden`}
      >
        <div variant="pills" className={`${styles.menuName} me-auto`}>
          <div
            className={`${styles.nvNme}`}
            onClick={() => handleBasicClick("tab1")}
            active={basicActive === "tab1"}
          >
            Overview
          </div>

          <div
            className={`${styles.nvNme}`}
            onClick={() => handleBasicClick("tab3")}
            active={basicActive === "tab3"}
          >
            Syllabus
          </div>
          <div
            className={`${styles.nvNme}`}
            onClick={() => handleBasicClick("tab2")}
            active={basicActive === "tab2"}
          >
            Benefits
          </div>
          <div
            className={`${styles.nvNme}`}
            onClick={() => handleBasicClick("tab4")}
            active={basicActive === "tab4"}
          >
            Masters
          </div>

          <div
            className={`${styles.nvNme}`}
            onClick={() => handleBasicClick("tab5")}
            active={basicActive === "tab5"}
          >
            Placement &amp; Reviews
          </div>

          <div
            className={`${styles.nvNme}`}
            onClick={() => handleBasicClick("tab6")}
            active={basicActive === "tab6"}
          >
            Talk to Expert
          </div>
        </div>
      </header>
      <div className="courseStickyMenu">
        <div id="tab1" show={basicActive === "tab1"}>
          <section className={`${styles.sectionContent01} sectionGlobal`}>
            <div className="container">
              <div className="row">
                <div className="col-md-7">
                  <div className={`${styles.box01}`}>
                    <HeadingContent
                      heading="About the <span>Course</span>"
                      pagragraph="<p> These short term courses are best for skill enhancement, candidates perform 
                  and applies advance formulas and use the software applications effectively to perform day to
                   day accounting.
                   The course focuses on the measurement and reporting of the asset side of the
                  balance sheets as well as the measurement of revenues and expenses on income statements.
                   The candidate also receives professional knowledge of Tally Prime which is advanced 
                   accounting software that is used to keep track of accounts. It helps records all 
                   types of accounting including invoicing, receipt notes, inventory management, 
                   sale records, credit note, etc. </p>
                  
                  <p>The candidate also acquires an extensive understanding of all practical aspects
                   of taxation. Last but not least, with the MS Office knowledge, the candidate
                    can work in Office as Computer Operator, Computer Office
                  Administrator, etc.</p>
                  "
                    />
                  </div>
                </div>
                <div className="col-md-5">
                  <KeyHighLight />
                </div>
              </div>
            </div>
          </section>
          <section className="p-5 my-3" style={{background:"#194AEA"}}>
              <div className="container">
                <div className="row">
                  <h1 className="text-light">CV SKILLS ADVANTAGE</h1>
                  <hr />
                  <p className="text-justify text-light">and the current academic course curriculum. To address this gap, we have dedicated ourselves to bridging it in a seamless manner. Our focus is to help students acquire industry-relevant skill sets by offering experiential learning opportunities. Our programs are designed and delivered by industry experts, ensuring that students receive practical knowledge and skills that align with the needs of the job market.</p>
                  <div className="col-md-4 mt-3">
                    <div className="h-100 rounded-3 border shadow p-4" style={{background:"#fff"}}>
                      <ul>
                        <li><b>1. Experienced Faculties</b></li>
                        <li><b>2. ‘e’ Taxation Expert</b></li>
                        <li><b>3. Seminars &amp; Webinar</b></li>
                        <li><b>4. Industry Base Projects Live Projects</b></li>
                      </ul>
                    </div>
                  </div>
                  <div className="col-md-4 mt-3">
                    <div className="h-100 rounded-3 border shadow p-4" style={{background:"#fff"}}>
                      <ul>
                        <li><b>5. In-house “E-Mitra”-LMS ERP Support</b></li>
                        <li><b>6. Simulation Software</b></li>
                        <li><b>7. Pre-Recorded Class Session</b></li>
                        <li><b>8. Pro-AJE Accounting Perfection</b></li>
                      </ul>
                    </div>
                  </div>
                  <div className="col-md-4 mt-3">
                    <div className="h-100 rounded-3 border shadow p-4" style={{background:"#fff"}}>
                      <ul>
                        <li><b>9. In-house R &amp; D</b></li>
                        <li><b>10. Direct Connect to Faculty</b></li>
                        <li><b>11. PDP Classes</b></li>
                        <li><b>12. Pre-Recorded Assig. Solution</b></li>
                      </ul>
                    </div>
                  </div>

                </div>
              </div>
            </section>
            <section className="mt-5 p-4" style={{background:"#f5f5f5"}}>
              <div className="container">
                <div className="row align-items-center">
                  <div className="col-md-12">
                    <h1>E-MITRA: An LMS that prioritizes the engagements</h1>
                    <hr />
                    <p>LMS stands for Learning Management System. LMS enables a better Learning Environment. Helps us to deliver adequate & updated educational content, as well as monitor student progress and performance. It also facilitates communication between teachers and students, provides online forums, and allows for the exchange of information and resources content for E-mitra page</p>
                  </div>
                  <div className="col-md-6 mt-4">
                    <h1>Pro-AJE:</h1>
                    <p>Proficiency- in Accounting Journal Entries is a Value-Added Offering for Learners. The Learner gets to practice above 2,000 Journal Entries to practice the routine & get a good holding with the following benefits:</p>
                    <p>Pro-AJE has been developed for the learners to get the On-Job Training Experience. A Learner is given an Opening Balance Sheet & Closing Balance Sheet of a company with various nature of accounting entries taking place around the year to practice himself.</p>
                    <p>These Pro-AJE build up the confidence of the Learners. During their initial days of the JOB, the Learner quickly adjusts himself to any nature of business the employer is into. Win-Win Situations for both the Employer & the Learner.</p>
                    <ul>
                      <li>1. Chronological order of occurrence.</li>
                      <li>2. Decreases chances of errors </li>
                      <li>3. Provides information about each transaction </li>
                      <li>4. Reduces spending efforts & time</li>
                    </ul>
                  </div>
                  <div className="col-md-6 mt-4">
                    <Image src='/course/lmsimg.png' height={500} width={600} alt="lmsimg" className="mainlms"/>
                  </div>
                </div>
              </div>
            </section>
        </div>

        <div id="tab3" show={basicActive === "tab3"}>
          <section
            className={`${styles.acordanContaner} ${styles.Course04} sectionGlobal`}
          >
            <div className="container">
              <div className="row">
                <div className="col-md-8">
                  <div className={`${styles.lftBox}`}>
                    <ButtonContainer courseName="Skill Enhancement Program" />

                    <div>
                      <h3>Certified Account Executive</h3>
                      <CustomAccordian contents={contents} />
                    </div>
                  </div>
                </div>
                <div className="col-md-4">
                  <div className={`${styles.rghtBox}`}>
                    <CourseContent />
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
        <div id="tab2" show={basicActive === "tab2"}>
          <Pointers />
        </div>
        <Section07 />
        <div id="tab4" show={basicActive === "tab4"}>
          <section className={`${styles.masterVideo} masterVideo`}>
            <MasterVideo />
          </section>
        </div>

        <div id="tab5" show={basicActive === "tab5"}>
          <section className="basikCont sectionGlobal">
            <div className="container">
              <div className="row">
                <div className="col-md-12">
                  <BasikCustomTab />
                </div>
              </div>
            </div>
          </section>
        </div>

        <div id="tab6" show={basicActive === "tab6"}>
          <section className="aboutEnrollSection">
            <TalkComponentGlobal
              Heading="Want to see yourself Job-Ready? "
              buttonName="Enroll Now"
            />
          </section>
        </div>
      </div>
      <Footer />
    </>
  );
};

export default Course_4;
