import React from "react";
import styles from "../courses/Course.module.css";

export const CourseContent = () => {
  return (
    <>
      <ul>
        <li>
          <span className={`${styles.circleBox}`}>8k</span>
          <span>Students Placed</span>
        </li>
        <li>
          <span className={`${styles.circleBox}`}>12k+</span>
          <span>Live Sessions</span>
        </li>

        <li>
          <span className={`${styles.circleBox}`}>1.6k+</span>
          <span>Video Tutorial</span>
        </li>

        <li>
          <span className={`${styles.circleBox}`}>600</span>
          <span>Webinars</span>
        </li>
        <li>
          <span className={`${styles.circleBox}`}>450</span>
          <span>Industry Projects</span>
        </li>
        <li>
          <span className={`${styles.circleBox}`}>600</span>
          <span>Assignment </span>
        </li>
      </ul>
    </>
  );
};

export default CourseContent;
