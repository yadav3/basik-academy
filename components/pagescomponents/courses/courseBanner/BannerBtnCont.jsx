import React from "react";
import { useEffect, useState } from "react";

import styles from "../Course.module.css";
import AnchorButton from "../../../globals/button/AnchorButton";
import FormModal from "../../../globals/modals/FormModal";

export const BannerBtnCont = () => {
  const [showModal, setShowModal] = useState(false);

  const handleShowModal = () => {
    setShowModal(true);
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  useEffect(() => {}, [showModal]);

  return (
    <>
      <a className="talkExBtn anchorbtnStyle" onClick={handleShowModal}>
        Apply Now
      </a>
      <div className="talkSection">
        <FormModal show={showModal} handleCloseModal={handleCloseModal} />
      </div>
    </>
  );
};

export default BannerBtnCont;
