import React, { useState, useEffect } from "react";
import Header from "../../../globals/header/Header";
import CourseBanner from "../courseBanner/CustomBanner";
import HeadingContent from "../../../globals/headingContent/HeadingContent";
import KeyHighLight from "../KeyHighLight";
import GetCourse from "../GetCourseSection/GetCourse";
import ButtonContainer from "../acordianSection/ButtonContainer";
import CustomAccordian from "../acordianSection/CustomAccordian";
import TalkComponentGlobal from "../../../globals/talkExpertLink/TalkComponentGlobal";
import Footer from "../../../globals/footer/Footer";
import styles from "../Course.module.css";
import { Accordion } from "react-bootstrap";
import CourseContent from "../CourseContent";
import MasterVideo from "../../faculty/MasterVideo";
import Certificate from "../../../globals/certificate/Certificate";
import BasikCustomTab from "../../../globals/basiksPride/BasikCustomTab";
import Fees from "../../../globals/fees/Fees";
import { Container } from "react-bootstrap";
import { Tab, Row, Col, Nav } from "react-bootstrap";
import ProductsCard from "../../../globals/ourCourses/ProductsCard";
import Image from "next/image";
import { Pointers } from "../../../globals/mainPointers/Pointers";
import Section07 from "../../about/Section07";

export const Course_2 = () => {
  const semstar01 = [
    {
      header: "Module-1",
      body: `
      <p><b>Computer Applications for Business Technology:</b></p>
      <div><b>Technology of Modern Computer System :</b></div>
      <p>Functionalities of a Computer, Usage of Computers, Computer System Unit Descriptions & Types, Computer Generations, Computer Components (Input, Output, Processing, Storage Devices), Data Measurements, Computer Ports, Hardware, Software, Different Operating Platforms, Mobile Operating Systems, Networking, Internet & Intranet, Virus & Different Virus Protection Software</p>

      `,
    },
    {
      header: "Module-2",
      body: `
      <div><b> Financial Accounting:</b></div>
      <p>Introduction of Accounting, Accounting Principle, Process of Accounting, Accounting Terminology, Business Mathematics for Accounting, Accounting Standards, Accounting Documents & Vouchers, Rules of Accounting – Golden Rules – Modern Approach Rules, Method of Accounting under Companies Act & Income Tax Act, Journal Book, Ledgers, Trial Balance, Trading A/c, Profit & Loss A/c, Accounting for Taxation – GST, TDS, Modern Approach to Bank Reconciliation, Depreciation and Rectification of Errors.
      </p>
      `,
    },
    {
      header: "Module-3",
      body: `
      <div><b>Tally Prime (Accounts & Inventory):</b></div>
      <p>Tally Prime- Vol. 1 (Accounts Only) History of Tally, Company Creation & Alteration, Voucher Creation & Alteration, Deletion, Primary Group, Secondary Group, Creation of Secondary Group, Alteration of Group, Voucher Transactions, Report Generation Day Book, Ledgers, Bank Book, Trial Balance, Trading & Profit & Loss A/c, Day Book Functions, Budget and Control, Cost Category and Cost Center, Security Maintain different Types, Banking (Cheque Entry) Reconciliation, Backup of Data, Restore of Data, Exporting Different Reports in Excel Format, PDF Format, Printing of Tally Accounting Reports i.e. Day Book, Vouchers, Ledgers, Cash Book, Bank Book, Trial Balance, Trading A/c, Profit & Loss, Balance Sheet.</p>

      <div><b>Tally Prime- VOL-2 (Accounts & Inventory)</b></div>
      <p>Company Creation with Accounts & Inventory Features, 
      Voucher Creation, Alteration & Deletion, Day Book 
      Functions, Process of Maintaining Inventory, Maintaining 
      Multiple Godowns, Actual & Bill QTY, Batch-wise Stock 
      Maintain, Transfer of Material, Order Processing, Multiple 
      Price Level, Currency Creation (Export – Country), Physical 
      Voucher, Manufacture Voucher, Bill of Material, Job Costing, 
      Interest Calculation, POS, Backup Restore, Exporting Tally 
      Inventory Reports in Different Formats (Excel, PDF, JPEG), 
      Printing of Tally Inventory Reports - Day Book, Invoice, 
      Godown Report, Party Ledgers, Trial Balance, Profit & Loss, 
      Balance Sheet.
      <br><b class="color1">Practical Assignment</b>
      <br><b class="color1">Project work on Tally prime (Accounts and Inventory)</b>
      </p>
      `,
    },
    {
      header: "Module-4",
      body: `
      <div><b>Goods & Services Tax (GST):</b></div>
      <p>Introduction to GST, GST ACT- Meaning, Terminologies, Goods Included under GST, Services included under GST, HSN/ SAC, Types of GST (IGST, CGST, SGST, UTGST), Cess Under GST, Taxable event in GST, Registration, TRN and ARN number, 15-digit GSTIN, GST Certificate, Composition Scheme Under GST, Accounting Entries under GST, State Codes and rates, Forms under GST, Reverse Charge Mechanism, Tax Invoice, Sales Ledgers, Purchase ledgers, Bill of Supply, E-way Bill and movement of Goods, E-way Bill GST Portal, E-invoicing, Calculation of GST, Input Tax Credit(ITC), Adjustments for CGST, SGST and UTGST </p>
      `,
    },
    {
      header: "Module-5",
      body: `
      <div><b>Direct Taxes:</b></div>
      <p>Introduction to Direct Tax, Terminologies used in Income Tax Act 1961, Type of Residential Status, Exempted Income, PAN and TAN, How to apply PAN – TAN through NSDL Portal, Linking of PAN and Aadhaar, Income Tax – Tax Slab Rates, Income from Salary, Provident Fund, SPF, RPF, URPF, PPF, Interest (under section 234A, 234B, 234C) and Rebate, Tax Deduction at Sources(TDS), TDS under Salary, Due dates (TDS deposit and returns) TDS Certificate/From 16, Deductions under Chapter VI ,Computation of Taxable Income and Tax Liability, Online Income Tax Return portal Registration and Income Tax return e-filing ( ITR-1).</p>
      `,
    },
    {
      header: "Module-6",
      body: `
      <div><b>English Communication & Personality Development</b></div>
      <p>What is Communication? Types of Communication, Interpersonal Communication, Process of Communication, Postulates and barriers of Communication, Linkage between Communication and Development. Probing verbal Communication, Becoming an effective speaker, Initiating Conversation, Learning kinesics, Role of the society, Importance and Types of Non-Verbal Communication, Presenting yourself, Differentiating listening and hearing, Understanding L-S-R-W, Misconceptions and barriers of listening, Tools to improve Communication skills and Personality development. </p>
      `,
    },
  ];

  const [show, setShow] = useState(false);
  const controlNavbar = () => {
    if (window.scrollY > 300) {
      setShow(true);
    } else {
      setShow(false);
    }
  };

  useEffect(() => {
    window.addEventListener("scroll", controlNavbar);
    return () => {
      window.removeEventListener("scroll", controlNavbar);
    };
  }, []);

  const [basicActive, setBasicActive] = useState("tab1");
  const handleBasicClick = (id) => {
    const element = document.getElementById(id);
    if (element === basicActive) {
      return;
    }

    setBasicActive(element);

    window.scrollTo({
      behavior: "smooth",
      top: element.offsetTop - 80,
    });
  };
  return (
    <>
      <Header />

      <CourseBanner
        banerImage="/banner/courses/year-course-banner.jpg"
        title01="Professional Courses"
        title02="Short Term Courses"
        title03=""
        imgTitle="/course/staricon.png"
        title04="6996 Ratings"
        title05="3500+ Learners"
        title06="An in-depth conceptual knowledge and application skills in the Short Term Courses.
        "
        VideoPath="/video/GstVideoFor.mp4"
      />
      <header
        className={`${styles.stickyMenuContainer} ${show && "SticyMenuActive"
          } hidden`}
      >
        <div variant="pills" className={`${styles.menuName} me-auto`}>
          <div
            className={`${styles.nvNme}`}
            onClick={() => handleBasicClick("tab1")}
            active={basicActive === "tab1"}
          >
            Overview
          </div>
          <div
            className={`${styles.nvNme}`}
            onClick={() => handleBasicClick("tab7")}
            active={basicActive === "tab7"}
          >
            Short Term Courses
          </div>
          <div
            className={`${styles.nvNme}`}
            onClick={() => handleBasicClick("tab2")}
            active={basicActive === "tab2"}
          >
            Benefits
          </div>
          <div
            className={`${styles.nvNme}`}
            onClick={() => handleBasicClick("tab4")}
            active={basicActive === "tab4"}
          >
            Masters
          </div>

          <div
            className={`${styles.nvNme}`}
            onClick={() => handleBasicClick("tab5")}
            active={basicActive === "tab5"}
          >
            Reviews
          </div>

          <div
            className={`${styles.nvNme}`}
            onClick={() => handleBasicClick("tab6")}
            active={basicActive === "tab6"}
          >
            Talk to Expert
          </div>
        </div>
      </header>
      <div className="courseStickyMenu">
        <div id="tab1" show={basicActive === "tab1"}>
          <section className={`${styles.sectionContent01} sectionGlobal`}>
            <div className="container">
              <div className="row">
                <div className="col-md-7">
                  <div className={`${styles.box01}`}>
                    <HeadingContent
                      heading="About the <span>Short Term Courses</span>"
                      pagragraph="<p>Short Courses can be a valuable investment in your professional development. They can help you stay up-to-date with the latest industry trends and best practices, as well as expand your knowledge and skills in a specific area.</p>
                      <p>Short courses are typically designed to provide targeted training on a specific topic or skill set. They are often more affordable and require less time commitment than longer programs, making them a great option for professionals who want to fill gaps in their knowledge or skills without committing to a full time program.</p>
                      <p>Skill Fulfillment Courses are particularly useful for professionals in business-related fields. These courses covers a range of topics, such as financial management, inventory control, and payroll processing. By taking these courses, professionals can gain the skills and knowledge they need to excel in their current roles or advance their careers. </p>"
                    />
                  </div>
                </div>
                <div className="col-md-5">
                  <KeyHighLight />
                </div>
              </div>
            </div>
          </section>
          <section className="p-5 my-3" style={{ background: "#194AEA" }}>
            <div className="container">
              <div className="row">
                <h1 className="text-light">CV SKILLS ADVANTAGE</h1>
                <hr />
                <p className="text-justify text-light">and the current academic course curriculum. To address this gap, we have dedicated ourselves to bridging it in a seamless manner. Our focus is to help students acquire industry-relevant skill sets by offering experiential learning opportunities. Our programs are designed and delivered by industry experts, ensuring that students receive practical knowledge and skills that align with the needs of the job market.</p>
                <div className="col-md-4 mt-3">
                  <div className="h-100 rounded-3 border shadow p-4" style={{ background: "#fff" }}>
                    <ul>
                      <li><b>1. Experienced Faculties</b></li>
                      <li><b>2. ‘e’ Taxation Expert</b></li>
                      <li><b>3. Seminars &amp; Webinar</b></li>
                      <li><b>4. Industry Base Projects Live Projects</b></li>
                    </ul>
                  </div>
                </div>
                <div className="col-md-4 mt-3">
                  <div className="h-100 rounded-3 border shadow p-4" style={{ background: "#fff" }}>
                    <ul>
                      <li><b>5. In-house “E-Mitra”-LMS ERP Support</b></li>
                      <li><b>6. Simulation Software</b></li>
                      <li><b>7. Pre-Recorded Class Session</b></li>
                      <li><b>8. Pro-AJE Accounting Perfection</b></li>
                    </ul>
                  </div>
                </div>
                <div className="col-md-4 mt-3">
                  <div className="h-100 rounded-3 border shadow p-4" style={{ background: "#fff" }}>
                    <ul>
                      <li><b>9. In-house R &amp; D</b></li>
                      <li><b>10. Direct Connect to Faculty</b></li>
                      <li><b>11. PDP Classes</b></li>
                      <li><b>12. Pre-Recorded Assig. Solution</b></li>
                    </ul>
                  </div>
                </div>

              </div>
            </div>
          </section>

        </div>

        <div id="tab7" show={basicActive === "tab7"}>
          <section>
            <div className={`${styles.hdingsection}`}>
              <h1 className={`${styles.title_ourcourse} text-center`}>
                Our <span>Courses</span>
              </h1>
              <h5>Get Certified, Get Ahead with Our Programs</h5>
              <ul>
                <li>
                  <Image
                    className="d-block"
                    width={15}
                    height={15}
                    src="/icons/icon1.png"
                    alt="Career"
                  />
                  <span>Master Classes from University</span>
                </li>
                <li>
                  <Image
                    className="d-block"
                    width={15}
                    height={15}
                    src="/icons/icon1.png"
                    alt="Career"
                  />
                  <span>Career Support</span>
                </li>
              </ul>
            </div>
            <Container>
              <Row>
                <div className="row">
                  <div className="col-md-4 mb-3">
                    <ProductsCard
                      CardImg="/course/course-image-4.png"
                      Title01="Skill Enhancement Program"
                      TitleFees="Fee: 699"
                      Title02="PERFECT OFFICE (MS OFFICE 2019)"
                      Pagragraph="An in-depth knowledge of MS Office, 
                            means you can make better presentations, use more 
                            features in Word & Excel and thus make your mark 
                            on work in your style."
                      title04="6590 Ratings"
                      title05="3900+ Learners"
                      NoYear="Duration 2 Months"
                      NoHour="60 Hours"
                      whoCanDo="Who Can Do :"
                      whoCanText="businessman, student, Executives, managers, Housewives, teachers almost everyone"
                      AncrName="Know More"
                    />
                    <div
                      className={`${styles.infoButton}`}
                      variant="primary"
                    ></div>
                  </div>
                  <div className="col-md-4 mb-3">
                    <ProductsCard
                      CardImg="/course/course-image-5.png"
                      Title01="Skill Enhancement Program"
                      TitleFees="Fee: 499"
                      Title02="MX-EXCEL INTERMEDIATE"
                      Pagragraph="Advance Excel allows one to organise the gathered data, 
                            analyse, interpret, and present the information in an easily 
                            understandable form. Thus, for Employability perspective it
                             sharpens skills sets, improve efficiency & Productivity"
                      title04="5890 Ratings"
                      title05="2900+ Learners"
                      NoYear="Duration 1 Months"
                      NoHour="25 Hours"
                      whoCanDo="Who Can Do :"
                      whoCanText="Businessman, Student, Executives, Managers, Accountants"
                      AncrName="Know More"
                    />
                  </div>
                  <div className="col-md-4 mb-3">
                    <ProductsCard
                      CardImg="/course/course-image-6.png"
                      Title01="Skill Enhancement Program"
                      TitleFees="Fee: 499"
                      Title02="EXCEL IN MS-EXCEL (ADVANCE MS-EXCEL)"
                      Pagragraph="Tally Prime is considered to be a Reliable, 
                            Efficient, Secure & Future Ready Accounting Software & is most 
                            popular Accounting Software in Business Industry for doing 
                            all Accounting works, Banking Transactions & Auditing."
                      title04="4980 Ratings"
                      title05="2499+ Learners"
                      NoYear="Duration 1 Months"
                      NoHour="25 Hours"
                      whoCanDo="Who Can Do :"
                      whoCanText="Businessman, Student, Executives, Managers, Accountants"
                      AncrName="Know More"
                    />
                  </div>
                </div>
                {/* another coures start */}
                <div className="row">
                  <div className="col-md-4 mb-3">
                    <ProductsCard
                      CardImg="/course/accounts-inventory.png"
                      Title01="Skill Enhancement Program"
                      TitleFees="Fee: 999"
                      Title02="TALLY PRIME (ACCOUNTS & INVENTORY)"
                      Pagragraph="An in-depth knowledge of MS Office, 
                            means you can make better presentations, use more 
                            features in Word & Excel and thus make your mark 
                            on work in your style."
                      title04="6590 Ratings"
                      title05="3900+ Learners"
                      NoYear="Duration 1 Months"
                      NoHour="35 Hours"
                      whoCanDo="Who Can Do :"
                      whoCanText="Businessman, Student, Executives, Managers, Accountants"
                      AncrName="Know More"
                    />
                    <div
                      className={`${styles.infoButton}`}
                      variant="primary"
                    ></div>
                  </div>
                  <div className="col-md-4 mb-3">
                    <ProductsCard
                      CardImg="/course/taxation-payroll.png"
                      Title01="Skill Enhancement Program"
                      TitleFees="Fee: 999"
                      Title02="TALLY PRIME (TAXATION & PAYROLL)"
                      Pagragraph="Advance Excel allows one to organise the gathered data, 
                            analyse, interpret, and present the information in an easily 
                            understandable form. Thus, for Employability perspective it
                             sharpens skills sets, improve efficiency & Productivity"
                      title04="5890 Ratings"
                      title05="2900+ Learners"
                      NoYear="Duration 1 Months"
                      NoHour="35 Hours"
                      whoCanDo="Who Can Do :"
                      whoCanText="Businessman, Student, Executives, Managers, Accountants"
                      AncrName="Know More"
                    />
                  </div>
                  <div className="col-md-4 mb-3">
                    <ProductsCard
                      CardImg="/course/tally-prime-complete.png"
                      Title01="Skill Enhancement Program"
                      TitleFees="Fee: 1499"
                      Title02="TALLY SAVVY (TALLY PRIME COMPLETE)"
                      Pagragraph="Tally Prime is considered to be a Reliable, 
                            Efficient, Secure & Future Ready Accounting Software & is most 
                            popular Accounting Software in Business Industry for doing 
                            all Accounting works, Banking Transactions & Auditing."
                      title04="4980 Ratings"
                      title05="2499+ Learners"
                      NoYear="Duration 2 Months"
                      NoHour="70 Hours"
                      whoCanDo="Who Can Do :"
                      whoCanText="Businessman, Student, Executives, Managers, Accountants"
                      AncrName="Know More"
                    />
                  </div>
                </div>
                <div className="d-flex justify-content-between">
                  <p className={`${styles.aboutCoursecombo}`}>
                    ** Any combo (&gt;=2 programs) shall attract 10% off
                  </p>
                  <p className={`${styles.aboutCourshags}`}>
                    #alagwahijoshikhesahi
                  </p>
                </div>
              </Row>
            </Container>
          </section>
        </div>
        <section className="mt-5 p-4" style={{ background: "#f5f5f5" }}>
          <div className="container">
            <div className="row align-items-center">
              <div className="col-md-12">
                <h1>E-MITRA: An LMS that prioritizes the engagements</h1>
                <hr />
                <p>LMS stands for Learning Management System. LMS enables a better Learning Environment. Helps us to deliver adequate & updated educational content, as well as monitor student progress and performance. It also facilitates communication between teachers and students, provides online forums, and allows for the exchange of information and resources content for E-mitra page</p>
              </div>
              <div className="col-md-6 mt-4">
                <h1>Pro-AJE:</h1>
                <p>Proficiency- in Accounting Journal Entries is a Value-Added Offering for Learners. The Learner gets to practice above 2,000 Journal Entries to practice the routine & get a good holding with the following benefits:</p>
                <p>Pro-AJE has been developed for the learners to get the On-Job Training Experience. A Learner is given an Opening Balance Sheet & Closing Balance Sheet of a company with various nature of accounting entries taking place around the year to practice himself.</p>
                <p>These Pro-AJE build up the confidence of the Learners. During their initial days of the JOB, the Learner quickly adjusts himself to any nature of business the employer is into. Win-Win Situations for both the Employer & the Learner.</p>
                <ul>
                  <li>1. Chronological order of occurrence.</li>
                  <li>2. Decreases chances of errors </li>
                  <li>3. Provides information about each transaction </li>
                  <li>4. Reduces spending efforts & time</li>
                </ul>
              </div>
              <div className="col-md-6 mt-4">
                <Image src='/course/lmsimg.png' height={500} width={600} alt="lmsimg" className="mainlms" />
              </div>
            </div>
          </div>
        </section>
        <div id="tab2" show={basicActive === "tab2"}>
          <Pointers />
        </div>
        <Section07 />
        <div id="tab4" show={basicActive === "tab4"}>
          <section className={`${styles.masterVideo} masterVideo`}>
            <MasterVideo />
          </section>
        </div>

        <div id="tab5" show={basicActive === "tab5"}>
          <section className="basikCont sectionGlobal">
            <div className="container">
              <div className="row">
                <div className="col-md-12">
                  <BasikCustomTab />
                </div>
              </div>
            </div>
          </section>
        </div>

        <div id="tab6" show={basicActive === "tab6"}>
          <section className="aboutEnrollSection">
            <TalkComponentGlobal
              Heading="Want to see yourself Job-Ready? "
              buttonName="Enroll Now"
            />
          </section>
        </div>
      </div>

      <Footer />
    </>
  );
};

export default Course_2;
