import React, { useState, useEffect } from "react";
import Header from "../../../globals/header/Header";
import CourseBanner from "../courseBanner/CustomBanner";
import HeadingContent from "../../../globals/headingContent/HeadingContent";
import KeyHighLight from "../KeyHighLight";
import GetCourse from "../GetCourseSection/GetCourse";
import ButtonContainer from "../acordianSection/ButtonContainer";
import CustomAccordian from "../acordianSection/CustomAccordian";
import TalkComponentGlobal from "../../../globals/talkExpertLink/TalkComponentGlobal";
import Footer from "../../../globals/footer/Footer";
import styles from "../Course.module.css";
import { Accordion } from "react-bootstrap";
import CourseContent from "../CourseContent";
import MasterVideo from "../../faculty/MasterVideo";
import Certificate from "../../../globals/certificate/Certificate";
import BasikCustomTab from "../../../globals/basiksPride/BasikCustomTab";
import Fees from "../../../globals/fees/Fees";
import { Container } from "react-bootstrap";
import { Tab, Row, Col, Nav } from "react-bootstrap";
import ProductsCard from "../../../globals/ourCourses/ProductsCard";
import Image from "next/image";
import { Pointers } from "../../../globals/mainPointers/Pointers";

export const Course_2 = () => {
  const semstar01 = [
    {
      header: "Fundamental of Business :",
      body: `
      <p>Evolution and Fundamental of Business, Introduction to Commerce, Introduction to Business, Profession, Employment, Difference Between Business, Profession & Employment, Objective of Business, Classification of Business Activities, Branches of Commerce, Forms of Business Organisations, Sole Proprietorship, Hindu Undivided Family Business, Partnership, Limited Liability Partnership (LLP), Cooperative Society, Joint Stock Company, Types of Companies, One Person Company, Private Company, Public Company.  Private Sector, Public Sector, Global Enterprises or Multinational Companies, Public Private Partnership.</p>

      `,
    },
    {
      header: "Fundamental of Accounting :",
      body: `
      <p>Introduction of Accounting, Accounting Principle, Process of Accounting, Accounting Terminology,  Rules of Accounting – Golden Rules – Modern Approach Rules, Recording Accounting Transactions in Journal Book, Adjustment Transactions in Journal Book,  Preparation of Ledgers – Books of Accounts, Preparation of Trial Balance, Final Account -  Preparation of  Trading A/c with the help of Trial Balance, Preparation of Profit & Loss A/c with the help of Trial Balance, Preparation of Final Balance Sheet of Company/Firm.</p>
      `,
    },
  ];

  const [show, setShow] = useState(false);
  const controlNavbar = () => {
    if (window.scrollY > 300) {
      setShow(true);
    } else {
      setShow(false);
    }
  };

  useEffect(() => {
    window.addEventListener("scroll", controlNavbar);
    return () => {
      window.removeEventListener("scroll", controlNavbar);
    };
  }, []);

  const [basicActive, setBasicActive] = useState("tab1");
  const handleBasicClick = (id) => {
    const element = document.getElementById(id);
    if (element === basicActive) {
      return;
    }

    setBasicActive(element);

    window.scrollTo({
      behavior: "smooth",
      top: element.offsetTop - 80,
    });
  };
  return (
    <>
      <Header />

      <CourseBanner
        banerImage="/banner/courses/year-course-banner.jpg"
        title01="Professional Diploma"
        title02="BRIDGE – Course For Non-Commerce Students."
        title06="An in-depth conceptual knowledge and application skills in the field of BRIDGE – Course."
      />
      <header
        className={`${styles.stickyMenuContainer} ${
          show && "SticyMenuActive"
        } hidden`}
      >
        <div variant="pills" className={`${styles.menuName} me-auto`}>
          <div
            className={`${styles.nvNme}`}
            onClick={() => handleBasicClick("tab1")}
            active={basicActive === "tab1"}
          >
            Overview
          </div>
          <div
            className={`${styles.nvNme}`}
            onClick={() => handleBasicClick("tab2")}
            active={basicActive === "tab2"}
          >
            Benefits
          </div>
          <div
            className={`${styles.nvNme}`}
            onClick={() => handleBasicClick("tab3")}
            active={basicActive === "tab3"}
          >
            Syllabus
          </div>
          <div
            className={`${styles.nvNme}`}
            onClick={() => handleBasicClick("tab4")}
            active={basicActive === "tab4"}
          >
            Masters
          </div>

          <div
            className={`${styles.nvNme}`}
            onClick={() => handleBasicClick("tab5")}
            active={basicActive === "tab5"}
          >
            Placement &amp; Reviews
          </div>

          <div
            className={`${styles.nvNme}`}
            onClick={() => handleBasicClick("tab6")}
            active={basicActive === "tab6"}
          >
            Talk to Expert
          </div>
        </div>
      </header>
      <div className="courseStickyMenu">
        <div id="tab1" show={basicActive === "tab1"}>
          <section className={`${styles.sectionContent01} sectionGlobal`}>
            <div className="container">
              <div className="row">
                <div className="col-md-7">
                  <div className={`${styles.box01}`}>
                    <HeadingContent
                      heading="About the <span>Bridge Course</span>"
                      pagragraph="<p>Bridge courses are designed to help students from non-traditional backgrounds bridge the gap between their prior knowledge and the skills required for a particular course or program. This course is designed to help non-commerce students develop a basic understanding of accounting principles.</p>
                      <p>By covering topics such as the Golden Rules and other basic principles of accounting, this course can provide non-commerce students with a foundation in accounting that they can build upon as they progress in their careers. Additionally, commerce background students can use this course to refresh their knowledge and reinforce their understanding of these principles.</p>
                      <p>Overall, bridge courses can be a useful tool for students who want to pursue a career in a field that is outside of their traditional area of study. By providing foundational knowledge and skills, these courses can help students bridge the gap and succeed in their chosen career path. </p>"
                    />
                  </div>
                </div>
                <div className="col-md-5">
                  <KeyHighLight />
                </div>
              </div>
            </div>
          </section>
          <section className="p-5 my-3" style={{background:"#194AEA"}}>
              <div className="container">
                <div className="row">
                  <h1 className="text-light">CV SKILLS ADVANTAGE</h1>
                  <hr />
                  <p className="text-justify text-light">and the current academic course curriculum. To address this gap, we have dedicated ourselves to bridging it in a seamless manner. Our focus is to help students acquire industry-relevant skill sets by offering experiential learning opportunities. Our programs are designed and delivered by industry experts, ensuring that students receive practical knowledge and skills that align with the needs of the job market.</p>
                  <div className="col-md-4 mt-3">
                    <div className="h-100 rounded-3 border shadow p-4" style={{background:"#fff"}}>
                      <ul>
                        <li><b>1. Experienced Faculties</b></li>
                        <li><b>2. ‘e’ Taxation Expert</b></li>
                        <li><b>3. Seminars &amp; Webinar</b></li>
                        <li><b>4. Industry Base Projects Live Projects</b></li>
                      </ul>
                    </div>
                  </div>
                  <div className="col-md-4 mt-3">
                    <div className="h-100 rounded-3 border shadow p-4" style={{background:"#fff"}}>
                      <ul>
                        <li><b>5. In-house “E-Mitra”-LMS ERP Support</b></li>
                        <li><b>6. Simulation Software</b></li>
                        <li><b>7. Pre-Recorded Class Session</b></li>
                        <li><b>8. Pro-AJE Accounting Perfection</b></li>
                      </ul>
                    </div>
                  </div>
                  <div className="col-md-4 mt-3">
                    <div className="h-100 rounded-3 border shadow p-4" style={{background:"#fff"}}>
                      <ul>
                        <li><b>9. In-house R &amp; D</b></li>
                        <li><b>10. Direct Connect to Faculty</b></li>
                        <li><b>11. PDP Classes</b></li>
                        <li><b>12. Pre-Recorded Assig. Solution</b></li>
                      </ul>
                    </div>
                  </div>

                </div>
              </div>
            </section>
            <section className="mt-5 p-4" style={{background:"#f5f5f5"}}>
              <div className="container">
                <div className="row align-items-center">
                  <div className="col-md-12">
                    <h1>E-MITRA: An LMS that prioritizes the engagements</h1>
                    <hr />
                    <p>LMS stands for Learning Management System. LMS enables a better Learning Environment. Helps us to deliver adequate & updated educational content, as well as monitor student progress and performance. It also facilitates communication between teachers and students, provides online forums, and allows for the exchange of information and resources content for E-mitra page</p>
                  </div>
                  <div className="col-md-6 mt-4">
                    <h1>Pro-AJE:</h1>
                    <p>Proficiency- in Accounting Journal Entries is a Value-Added Offering for Learners. The Learner gets to practice above 2,000 Journal Entries to practice the routine & get a good holding with the following benefits:</p>
                    <p>Pro-AJE has been developed for the learners to get the On-Job Training Experience. A Learner is given an Opening Balance Sheet & Closing Balance Sheet of a company with various nature of accounting entries taking place around the year to practice himself.</p>
                    <p>These Pro-AJE build up the confidence of the Learners. During their initial days of the JOB, the Learner quickly adjusts himself to any nature of business the employer is into. Win-Win Situations for both the Employer & the Learner.</p>
                    <ul>
                      <li>1. Chronological order of occurrence.</li>
                      <li>2. Decreases chances of errors </li>
                      <li>3. Provides information about each transaction </li>
                      <li>4. Reduces spending efforts & time</li>
                    </ul>
                  </div>
                  <div className="col-md-6 mt-4">
                    <Image src='/course/lmsimg.png' height={500} width={600} alt="lmsimg" className="mainlms"/>
                  </div>
                </div>
              </div>
            </section>
        </div>

        <div id="tab3" show={basicActive === "tab3"}>
          <section className={`${styles.acordanContaner} ${styles.Course02} `}>
            <div className="container">
              <div className="row">
                <div className="col-md-8">
                  <div className={`${styles.lftBox}`}>
                    <ButtonContainer courseName="Career Advancing Diploma Program " />
                    <Accordion defaultActiveKey="0">
                      <Accordion.Item eventKey="0" className="mainAccordion01">
                        <Accordion.Header>
                          Semester-1 (Fundamental of Business & Accounting)
                        </Accordion.Header>
                        <Accordion.Body>
                          <CustomAccordian contents={semstar01} />
                        </Accordion.Body>
                      </Accordion.Item>
                    </Accordion>
                  </div>
                </div>
                <div className="col-md-4">
                  <div className={`${styles.rghtBox}`}>
                    <CourseContent />
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>

        <div id="tab2" show={basicActive === "tab2"}>
          <Pointers />
        </div>

        <div id="tab4" show={basicActive === "tab4"}>
          <section className={`${styles.masterVideo} masterVideo`}>
            <MasterVideo />
          </section>
        </div>

        <div id="tab5" show={basicActive === "tab5"}>
          <section className="basikCont sectionGlobal">
            <div className="container">
              <div className="row">
                <div className="col-md-12">
                  <BasikCustomTab />
                </div>
              </div>
            </div>
          </section>
        </div>

        <div id="tab6" show={basicActive === "tab6"}>
          <section className="aboutEnrollSection">
            <TalkComponentGlobal
              Heading="Want to see yourself Job-Ready? "
              buttonName="Enroll Now"
            />
          </section>
        </div>
      </div>

      <Footer />
    </>
  );
};

export default Course_2;
