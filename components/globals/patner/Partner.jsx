import React from "react";
import HeadingContent from "../headingContent/HeadingContent";
import styles from "./Patner.module.css";
import Custom_Form from "./Custom_Pform";
import { useState } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { Autoplay, Pagination, Navigation } from "swiper";
import { Container, Row, Col } from "react-bootstrap";
import Image from "next/image";
import Link from "next/link";

export const Partner = () => {
  return (
    <>
      <section
        className={`${styles.contactContainer} contactPage sectionGlobal`}
      >
        <div className="container">
          <div className="row">
            <div className="col-md-12 text-center">
              <div className={`${styles.contLeft}`}>
                <HeadingContent
                  heading="Partners With Us</span>"
                  pagragraph="Join us today and let us help you unlock your potential. Leave your details/query
                                    in the form."
                />
                <Image
                  width={65}
                  height={65}
                  className="bannerImage rounded"
                  src="/contact/partner.png"
                  alt="partner"
                />
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className={`${styles.contactContainer01}`}>
        <div className="container mt-5">
          <div className="row align-items-center">
            <div className="col-md-6">
              <div className="d-flex">
                <h1 className="mb-3">
                  Join Hands <span className="text-primary">With Us</span>
                </h1>
              </div>
              <ul className={`${styles.leftsingpoints}`}>
                <li>1. Assured Longevity</li>
                <li>2. Huge Market Potential</li>
                <li>3. Tech Enabled Supports & Routines</li>
                <li>4. Industry Relevant Curriculum</li>
              </ul>
              <div className="mt-5">
                <h1 className="my-3">
                  Call doesn&apos;t feel enough{" "}
                  <span className="text-primary">Visit Us</span>
                </h1>
                <p>
                  At Collegevidya, it is our constant endeavour to provide great
                  customer experience. In case you require assistance, we have
                  created multiple ways to reach out to us.
                </p>
                <div className="mt-4 mb-4">
                  <a
                    className="btn-primary px-3 py-2"
                    href="tel:1800-309-2254"
                    style={{ borderRadius: "5px" }}
                  >
                    Call Now
                  </a>
                </div>
                <p className="ps-1">
                  <a
                    className="text-dark"
                    href="https://goo.gl/maps/TrzYNE2xCAmwrc6g8"
                  >
                    <svg
                      stroke="currentColor"
                      fill="currentColor"
                      strokeWidth="0"
                      viewBox="0 0 16 16"
                      height="1em"
                      width="1em"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        fillRule="evenodd"
                        d="M4 4a4 4 0 1 1 4.5 3.969V13.5a.5.5 0 0 1-1 0V7.97A4 4 0 0 1 4 3.999zm2.493 8.574a.5.5 0 0 1-.411.575c-.712.118-1.28.295-1.655.493a1.319 1.319 0 0 0-.37.265.301.301 0 0 0-.057.09V14l.002.008a.147.147 0 0 0 .016.033.617.617 0 0 0 .145.15c.165.13.435.27.813.395.751.25 1.82.414 3.024.414s2.273-.163 3.024-.414c.378-.126.648-.265.813-.395a.619.619 0 0 0 .146-.15.148.148 0 0 0 .015-.033L12 14v-.004a.301.301 0 0 0-.057-.09 1.318 1.318 0 0 0-.37-.264c-.376-.198-.943-.375-1.655-.493a.5.5 0 1 1 .164-.986c.77.127 1.452.328 1.957.594C12.5 13 13 13.4 13 14c0 .426-.26.752-.544.977-.29.228-.68.413-1.116.558-.878.293-2.059.465-3.34.465-1.281 0-2.462-.172-3.34-.465-.436-.145-.826-.33-1.116-.558C3.26 14.752 3 14.426 3 14c0-.599.5-1 .961-1.243.505-.266 1.187-.467 1.957-.594a.5.5 0 0 1 .575.411z"
                      ></path>
                    </svg>{" "}
                    Visit Us :{" "}
                  </a>
                  <a
                    className="textprimary"
                    href="https://goo.gl/maps/TrzYNE2xCAmwrc6g8"
                  >
                    {" "}
                    (10 AM to 7 PM)
                  </a>
                </p>
                <div className="d-flex gap-2 align-items-center mb-3">
                  <div>
                    <svg
                      stroke="currentColor"
                      fill="currentColor"
                      strokeWidth="0"
                      version="1"
                      viewBox="0 0 48 48"
                      enableBackground="new 0 0 48 48"
                      className="bg-light p-2"
                      fontSize="40"
                      height="1em"
                      width="1em"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <polygon
                        fill="#3F51B5"
                        points="44,19 30,30.7 30,7.3"
                      ></polygon>
                      <path
                        fill="#3F51B5"
                        d="M6,27v13h8V27c0-2.2,1.8-4,4-4h17v-8H18C11.4,15,6,20.4,6,27z"
                      ></path>
                      <polygon
                        fill="#3F51B5"
                        points="44,19 30,30.7 30,7.3"
                      ></polygon>
                      <path
                        fill="#3F51B5"
                        d="M6,27v13h8V27c0-2.2,1.8-4,4-4h17v-8H18C11.4,15,6,20.4,6,27z"
                      ></path>
                    </svg>
                  </div>
                  <div>
                    <p className="m-0 fw-bold">
                      <a
                        className="text-dark"
                        href="https://goo.gl/maps/TrzYNE2xCAmwrc6g8"
                      >
                        B-136, B Block, Sector 2, Noida, Uttar Pradesh 201301
                      </a>
                    </p>
                  </div>
                </div>
                <div className="d-flex mb-3">
                  <svg
                    stroke="currentColor"
                    fill="currentColor"
                    strokeWidth="0"
                    viewBox="0 0 1024 1024"
                    className="h1 me-2 textprimary"
                    height="1em"
                    width="1em"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path d="M928 160H96c-17.7 0-32 14.3-32 32v640c0 17.7 14.3 32 32 32h832c17.7 0 32-14.3 32-32V192c0-17.7-14.3-32-32-32zm-40 110.8V792H136V270.8l-27.6-21.5 39.3-50.5 42.8 33.3h643.1l42.8-33.3 39.3 50.5-27.7 21.5zM833.6 232L512 482 190.4 232l-42.8-33.3-39.3 50.5 27.6 21.5 341.6 265.6a55.99 55.99 0 0 0 68.7 0L888 270.8l27.6-21.5-39.3-50.5-42.7 33.2z"></path>
                  </svg>
                  <p className="m-0 fw-bold mt-3 contactP">
                    skills@collegevidya.com
                  </p>
                </div>
              </div>
            </div>
            <div className="col-md-6">
              <div className={`${styles.formContainer}`}>
                <Custom_Form />
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Partner;
