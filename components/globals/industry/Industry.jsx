import React from 'react';
import Image from "next/image";
import styles from './Industry.module.css'
import { Col, Container, Row } from "react-bootstrap";
import { Pointers } from '../mainPointers/Pointers';

const IndustryFunction = () => {

    return (
        <>
            <section>
                <Container>
                    <Row className="mt-5 align-items-center">
                        <Col md={4}>
                            <div className="imgage-hero px-0 d-none d-md-block">
                                <Image
                                    src="/bfsisector/b1.png"
                                    width="400"
                                    height="600"
                                    alt="value"
                                />
                            </div>
                            <div className="mx-auto px-0 d-block d-md-none">
                                <span>
                                    <Image
                                        src="/bfsisector/b7.png"
                                        width="400"
                                        height="250"
                                        alt="value"
                                    />
                                </span>
                            </div>
                        </Col>
                        <Col md={8}>
                            <div className="heading-box">
                                <h1>
                                    About <span> BFSI Sector</span>
                                </h1>
                            </div>
                            <p style={{ textAlign: "justify", lineHeight: "26px" }}>
                                India has created a favourable environment for the BFSI
                                (Banking, Financial Services, and Insurance) sector to thrive.
                                The interdependent factors that you mentioned, such as
                                government policy, active public/private involvement, robust
                                regulatory measures, and technological evolution, have all
                                contributed to the growth of the BFSI sector in recent years.
                            </p>
                            <p style={{ textAlign: "justify", lineHeight: "26px" }}>
                                The active involvement of public and private players in the
                                sector has also contributed to its growth. Banks, insurance
                                companies, and other financial institutions have made
                                significant investments in technology, expanded their reach into
                                rural areas, and developed innovative products and services to
                                cater to the needs of customers.
                            </p>
                            <p style={{ textAlign: "justify", lineHeight: "26px" }}>
                                The regulatory framework in India is robust and has been
                                strengthened in recent years with the introduction of new laws
                                and regulations. This has helped to build trust among customers
                                and investors, which has further fueled the growth of the BFSI
                                sector.
                            </p>
                            <p style={{ textAlign: "justify", lineHeight: "26px" }}>
                                Finally, technological evolution has played a significant role
                                in the growth of the BFSI sector in India. The widespread
                                adoption of mobile phones and the internet has enabled financial
                                institutions to reach a larger customer base, offer more
                                convenient services, and reduce costs. The emergence of fintech
                                companies has also disrupted the traditional BFSI sector,
                                forcing incumbents to innovate and adapt to changing customer
                                needs.
                            </p>
                            <p style={{ textAlign: "justify", lineHeight: "26px" }}>
                                Overall, India&apos;s favourable business environment, supported
                                by government policy, active public/private involvement, robust
                                regulatory measures, and technological evolution, has helped the
                                BFSI sector to register strong growth in recent years
                            </p>
                        </Col>
                    </Row>
                    <Row className="mt-5 align-items-center">
                        <Col className="order-2 order-md-1" md={7}>
                            <div className="heading-box">
                                <h1>
                                    Concept <span> & Need</span>
                                </h1>
                            </div>
                            <p style={{ textAlign: "justify", lineHeight: "26px" }}>
                                India is the second fastest growing economy in the world. The
                                financial sector is growing @ 30% and the demand for financial
                                professionals is increasing by leaps and bounds. For every
                                Chartered Accountant and MBA (Finance), there are at least 10
                                more people required in critical support functions in the
                                Finance sector. A study by the Ministry of Human Resources and
                                Development forecasts the shortage of Finance professionals by
                                more than 2 lakhs in 2025. The industry estimates say that
                                nearly 80% of all graduates in India are unemployable since they
                                do not have the requisite skill set.
                            </p>
                            <p>
                                While people in the Metros have access to quality education and
                                employment opportunities, they are not easily available to those
                                residing in the rural and semi-urban areas of the country.
                            </p>
                            <p>
                                The idea is to tap the markets in these areas, especially the B,
                                B+ and C category townships. This can be done by providing
                                courses by a company which has a pan Indian presence and strong
                                Industry alliances.
                            </p>
                        </Col>

                        <Col className="order-1 order-md-2" md={5}>
                            <Image
                                src="/bfsisector/b6.png"
                                width="510"
                                height="452"
                                alt="value"
                                style={{ borderRadius: "8px;" }}
                            />
                        </Col>
                    </Row>

                </Container>
            </section>
            <section className="bg-primary text-light p-4 my-4">
                <Container>
                    <Row>
                        <Col md={12}>
                            <div className='p-4'>
                                <h1>Scope in BFSI</h1>
                                <h5><b>(Banking, Financial Services & Insurance Sector)</b></h5>
                                <hr />
                                <p className="text-justify"><strong className='fs-5'>Increasing economic growth -</strong> The Indian economy has been growing steadily in recent years, providing opportunities for the BFSI sector to expand and support various industries.
                                    Increasing use of technology -  The BFSI sector has been embracing digital transformation, with increasing use of technology and automation to enhance operational efficiency and customer experience.</p>
                                <p className="text-justify"><strong className='fs-5'>Increasing disposable income -</strong> Rising disposable incomes have led to increased demand for financial products and services, such as loans, insurance, and investments.</p>
                                <p className="text-justify"><strong className='fs-5'>Strong regulatory framework -</strong> The regulatory framework in India is tightening its grip a day by day by linking multiple factors, thus, increase in awareness and necessity of governance amongst the public.</p>
                                <p className="text-justify">Multiple factors are driving the growth of this sector, such as digitization, advancement in technologies such as artificial intelligence and cognitive analytics, cybersecurity advancements, and the growth in the number of fintech</p>
                                <p className="text-justify">Businesses Will Remain Forever Till The Humans Exist” SO DO THE NEED OF FINANCE PROFESSIONALS.</p>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </section>
            <Pointers />
            <section className="p-5 my-3" style={{background:"#194AEA"}}>
              <div className="container">
                <div className="row">
                  <h1 className="text-light">CV SKILLS ADVANTAGE</h1>
                  <hr />
                  <p className="text-justify text-light">and the current academic course curriculum. To address this gap, we have dedicated ourselves to bridging it in a seamless manner. Our focus is to help students acquire industry-relevant skill sets by offering experiential learning opportunities. Our programs are designed and delivered by industry experts, ensuring that students receive practical knowledge and skills that align with the needs of the job market.</p>
                  <div className="col-md-4 mt-3">
                    <div className="h-100 rounded-3 border shadow p-4" style={{background:"#fff"}}>
                      <ul>
                        <li><b>1. Experienced Faculties</b></li>
                        <li><b>2. ‘e’ Taxation Expert</b></li>
                        <li><b>3. Seminars &amp; Webinar</b></li>
                        <li><b>4. Industry Base Projects Live Projects</b></li>
                      </ul>
                    </div>
                  </div>
                  <div className="col-md-4 mt-3">
                    <div className="h-100 rounded-3 border shadow p-4" style={{background:"#fff"}}>
                      <ul>
                        <li><b>5. In-house “E-Mitra”-LMS ERP Support</b></li>
                        <li><b>6. Simulation Software</b></li>
                        <li><b>7. Pre-Recorded Class Session</b></li>
                        <li><b>8. Pro-AJE Accounting Perfection</b></li>
                      </ul>
                    </div>
                  </div>
                  <div className="col-md-4 mt-3">
                    <div className="h-100 rounded-3 border shadow p-4" style={{background:"#fff"}}>
                      <ul>
                        <li><b>9. In-house R &amp; D</b></li>
                        <li><b>10. Direct Connect to Faculty</b></li>
                        <li><b>11. PDP Classes</b></li>
                        <li><b>12. Pre-Recorded Assig. Solution</b></li>
                      </ul>
                    </div>
                  </div>

                </div>
              </div>
            </section>
        </>
    );
}

export default IndustryFunction;
