import React from "react";
import styles from "../banner/Banner.module.css";
import Image from "next/image";

export const StaticBanner = (props) => {
  return (
    <>
      <section className={`${styles.bannerSection} banerSection`}>
        <Image
          className="d-block w-100"
          width={1983}
          height={700}
          src={props.BannerImage}
          alt="First slide"
        />
        <div className={`${styles.BanerTextBox} text-left`}>
          <h1
            style={{
              position: "absolute",
              top: "50%",
              color: "#fff",
              margin: "0 0 0 43px",
            }}
          >
            {props.Heading}
          </h1>
          <p
            style={{
              position: "absolute",
              top: "60%",
              color: "#fff",
              margin: "0 0 0 43px",
            }}
          >
            {props.Heading}
            {props.Para}
          </p>
        </div>
      </section>
    </>
  );
};

export default StaticBanner;
