import { useState } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { Autoplay, Pagination, Navigation } from "swiper";
import { Container, Row, Col } from "react-bootstrap";
import Image from "next/image";
import styles from "./Banner.module.css";
import Custom_Form from "../../pagescomponents/contact/Custom_Form";
import BannerForm from "./BannerForm";

export const Banner = () => {
  return (
    <section className={`${styles.banner_pad}`}>
      <Container fluid style={{ position: "relative" }}>
        <Row>
          <>
            <Col md={8} className="p-0 bannerformhero">

              <Swiper
                spaceBetween={30}
                centeredSlides={true}
                autoplay={{
                  delay: 4000,
                  disableOnInteraction: false,
                }}
                loop={true}
                pagination={{
                  clickable: true,
                }}
                navigation={false}
                modules={[Autoplay, Pagination, Navigation]}
                className="mySwiper"
              >
                <SwiperSlide>
                  <div className={styles.slide}>
                    <div className="imgage-hero px-0 d-none d-md-block">
                      <Image
                        width={1900}
                        height={1100}
                        className="bannerImage"
                        src="/banner/home/Homepage-banner-01.png"
                        alt="Homebanner1"
                      />
                    </div>
                    <div className="mx-auto px-0 d-block d-md-none">
                      <span>
                        <Image
                          width={418}
                          height={224}
                          className="bannerImage"
                          src="/banner/home/Homepage-banner-04.png"
                          alt="Homebanner2"
                        />
                      </span>
                    </div>
                  </div>
                </SwiperSlide>
                <SwiperSlide>
                  <div className={styles.slide}>
                    <div className="imgage-hero px-0 d-none d-md-block">
                      <Image
                        width={1900}
                        height={1100}
                        className="bannerImage"
                        src="/banner/home/Homepage-banner-02.png"
                        alt="Homebanner2"
                      />
                    </div>
                    <div className="mx-auto px-0 d-block d-md-none">
                      <span>
                        <Image
                          width={418}
                          height={224}
                          className="bannerImage"
                          src="/banner/home/Homepage-banner-05.png"
                          alt="Homebanner2"
                        />
                      </span>
                    </div>
                  </div>
                </SwiperSlide>
              </Swiper>
            </Col>
            <Col md={4} className="mt-5 bannerformhero bannerformheromain">
              <div className={styles.bannerFormContainer}>
                <div className="container-fluid" style={{ position: "relative" }}>
                    <BannerForm />
                </div>
              </div>
            </Col>
          </>

        </Row>
      </Container>
    </section>
  );
};
