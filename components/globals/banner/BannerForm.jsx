import React, { useEffect, useState } from "react";
import { Form, Button, Spinner, Toast, ToastContainer } from "react-bootstrap";
import { InputText } from "primereact/inputtext";
import { Dropdown } from "primereact/dropdown";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useRouter } from "next/router";
import styles from "./Banner.module.css";
import { BsArrowRight } from "react-icons/bs";
import {
  getCourseAPIKey,
  getSubCourses,
  generateStudentID,
  getStates,
  getCities,
  getLearningCenters,
  getCourseTypesAPIKey,
} from "../../../services/MiscService";

const BannerForm = () => {
  const router = useRouter();

  const [courseTypes, setCourseTypes] = useState([]);
  const [courses, setCourses] = useState([]);
  const [specializations, setSpecializations] = useState([]);
  const [states, setStates] = useState([]);
  const [cities, setCities] = useState([]);
  const [learningCentres, setLearningCentres] = useState([]);
  const [submittingLead, setSubmittingLead] = useState(false);
  const [showError, setShowError] = useState(false);
  const [errorStatement, setErrorStatement] = useState(null);

  const phoneval = /^[6789][0-9]{9}$/;

  const schema = yup.object().shape({
    fullName: yup.string().required("Please enter your full name."),
    email: yup
      .string()
      .email("Please enter a valid email address.")
      .required("Please enter your email address."),
    course_type: yup.string().required("Please select your course type."),
    course: yup.string().required("Please select your course."),
    subcourse: yup.string().required("Please select your specialization."),
    mobileNumber: yup
      .string()
      .matches(phoneval, "Please enter a valid 10-digit mobile no.")
      .required("Please enter your mobile number."),
    state: yup.string().required("Please select your state."),
    city: yup.string().required("Please select your City."),
    centresCount: yup.boolean(),
    learningCentre: yup.string().when("centresCount", {
      is: true,
      then: () => yup.string().required("Please select a Learning Center"),
    }),
  });

  useEffect(() => {
    getCourseTypesAPIKey().then((response) => {
      const CourseTypeObject = response.data;
      const allCourseTypes = Object.keys(CourseTypeObject).map((key) => {
        return {
          id: key,
          value: CourseTypeObject[key],
        };
      });
      setCourseTypes(allCourseTypes);
    });
  }, []);

  const {
    register,
    setValue,
    getValues,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: {
      centresCount: false,
    },
    mode: "onChange",
    resolver: yupResolver(schema),
  });

  useEffect(() => {
    if (getValues("course_type") && getValues("course_type") !== "") {
      getCourseAPIKey(getValues("course_type")).then((response) => {
        const CourseObject = response.data;
        const allCourses = Object.keys(CourseObject).map((key) => {
          return {
            id: key,
            value: CourseObject[key],
          };
        });
        setCourses(allCourses);
      });
    }
  }, [getValues("course_type")]);

  useEffect(() => {
    if (getValues("course") && getValues("course") !== "") {
      getSubCourses(getValues("course")).then((response) => {
        const SubCourseObject = response.data;
        const allSubCourses = Object.keys(SubCourseObject).map((key) => {
          return {
            id: key,
            value: SubCourseObject[key],
          };
        });
        setSpecializations(allSubCourses);
      });
    } else {
      setSpecializations([]);
    }
  }, [getValues("course")]);

  useEffect(() => {
    getStates()
      .then((response) => {
        setStates(response.data.state);
      })
      .catch(() => {
        setStates([]);
        setCities([]);
      });
  }, []);

  useEffect(() => {
    if (getValues("state") && getValues("state") !== "") {
      setCities([]);
      setLearningCentres([]);
      getCities(getValues("state"))
        .then((response) => {
          setCities(response.data.district);
        })
        .catch(() => {
          setCities([]);
          setLearningCentres([]);
        });
    }
  }, [getValues("state")]);

  useEffect(() => {
    if (
      getValues("state") &&
      getValues("city") &&
      getValues("state") !== "" &&
      getValues("city") !== ""
    ) {
      setLearningCentres([]);
      getLearningCenters(getValues("state"), getValues("city"))
        .then((response) => {
          if (response.data.length > 0) {
            setValue("centresCount", true, { shouldValidate: true });
          } else {
            setValue("centresCount", false, { shouldValidate: true });
          }
          setLearningCentres(response.data);
        })
        .catch(() => {
          setValue("centresCount", false, { shouldValidate: true });
          setLearningCentres([]);
        });
    }
  }, [getValues("city")]);

  const centreOptionTemplate = (option) => {
    return <div style={{ maxWidth: "400px" }}>{option.name}</div>;
  };

  const onSubmit = (data) => {
    setSubmittingLead(true);

    let formData = new FormData();
    formData.append("fullName", data.fullName);
    formData.append("email", data.email);
    formData.append("mobile", data.mobileNumber);
    formData.append("courseType", data.course_type);
    formData.append("course", data.course);
    formData.append("specialization", data.subcourse);
    formData.append(
      "learningCenter",
      data.learningCentre ? data.learningCentre : ""
    );
    formData.append("state", data.state);
    formData.append("city", data.city);
    formData.append("source", "College Vidya");
    formData.append("subSource", "CV-Skill Website");

    generateStudentID(formData)
      .then(() => {
        setSubmittingLead(false);
        window.location.href = "/thanku";
      })
      .catch((error) => {
        setSubmittingLead(false);
        setErrorStatement(error.response.data.message);
        setShowError(true);
      });
  };

  return (
    <Form className=" bannerformhero" onSubmit={handleSubmit(onSubmit)}>
      <div className={styles.forumsubt}>
        <div class="col-md-12 text-center ">
          <h2 class="fw-bold text-white fs-3 Enquirenow my-3">Enquire Now</h2>
        </div>
        <div className={styles.BannerFormmain}>
          <div className="col-lg-6 Mapheit">
            <Form.Group controlId="fullName.ControlInput1">
              <Form.Label style={{ color: "#fff", fontSize: "14px" }}>
                Full Name <span style={{ color: "white" }}>*</span>
              </Form.Label>
              <InputText
                className="herocontrol shadow-none  "
                type="text"
                placeholder="Enter your Full Name"
                {...register("fullName")}
              />
              {errors.fullName && (
                <p
                  className="text-warning position-absolute"
                  style={{ fontSize: "10px" }}
                >
                  {errors.fullName.message}
                </p>
              )}
            </Form.Group>
          </div>
          <div className="col-lg-6 Mapheit">
            <Form.Group controlId="course.ControlSelect1">
              <Form.Label style={{ color: "#fff", fontSize: "14px" }}>
                Select Course Type <span style={{ color: "white" }}>*</span>
              </Form.Label>
              <Dropdown
                className="mainformcontrol shadow-none w-100"
                value={getValues("course_type")}
                onChange={(e) =>
                  setValue("course_type", e.target.value, {
                    shouldValidate: true,
                  })
                }
                options={courseTypes}
                optionValue="id"
                optionLabel="value"
                placeholder="Select a Course Type"
                filter
              />
              {errors.course_type && (
                <p
                  className="text-warning position-absolute"
                  style={{ fontSize: "10px" }}
                >
                  {errors.course_type.message}
                </p>
              )}
            </Form.Group>
          </div>
        </div>
        <div className={styles.BannerFormmain}>
          <div className="col-lg-6 Mapheit">
            <Form.Group controlId="course.ControlSelect1">
              <Form.Label style={{ color: "#fff", fontSize: "14px" }}>
                Select Course <span style={{ color: "white" }}>*</span>
              </Form.Label>
              <Dropdown
                className="mainformcontrol shadow-none w-100"
                value={getValues("course")}
                onChange={(e) =>
                  setValue("course", e.target.value, { shouldValidate: true })
                }
                options={courses}
                optionValue="id"
                optionLabel="value"
                placeholder="Select a Course"
                filter
              />
              {errors.course && (
                <p
                  className="text-warning position-absolute"
                  style={{ fontSize: "10px" }}
                >
                  {errors.course.message}
                </p>
              )}
            </Form.Group>
          </div>
          <div className="col-lg-6 Mapheit">
            <Form.Group controlId="subcourse.ControlSelect1">
              <Form.Label style={{ color: "#fff", fontSize: "14px" }}>
                Select Specialization <span style={{ color: "white" }}>*</span>
              </Form.Label>
              <Dropdown
                className="mainformcontrol shadow-none w-100"
                value={getValues("subcourse")}
                onChange={(e) =>
                  setValue("subcourse", e.target.value, {
                    shouldValidate: true,
                  })
                }
                options={specializations}
                optionValue="id"
                optionLabel="value"
                placeholder="Select a Specialization"
                filter
              />
              {errors.subcourse && (
                <p
                  className="text-warning position-absolute"
                  style={{ fontSize: "10px" }}
                >
                  {errors.subcourse.message}
                </p>
              )}
            </Form.Group>
          </div>
        </div>

        <div className={styles.BannerFormmain}>
          <div className="col-lg-6 Mapheit">
            <Form.Group controlId="mobile.ControlInput1">
              <Form.Label style={{ color: "#fff", fontSize: "14px" }}>
                Mobile Number <span style={{ color: "white" }}>*</span>
              </Form.Label>
              <InputText
                keyfilter={"num"}
                className="w-100"
                type="tel"
                maxLength={10}
                placeholder="Enter your Mobile Number"
                {...register("mobileNumber")}
              />
              {errors.mobileNumber && (
                <p
                  className="text-warning position-absolute"
                  style={{ fontSize: "10px" }}
                >
                  {errors.mobileNumber.message}
                </p>
              )}
            </Form.Group>
          </div>
          <div className="col-lg-6 Mapheit">
            <Form.Group controlId="email.ControlInput1">
              <Form.Label style={{ color: "#fff", fontSize: "14px" }}>
                Email<span style={{ color: "white" }}>*</span>
              </Form.Label>
              <InputText
                className="w-100"
                type="email"
                placeholder="Enter your Email Address"
                {...register("email")}
              />
              {errors.email && (
                <p
                  className="text-warning position-absolute"
                  style={{ fontSize: "10px" }}
                >
                  {errors.email.message}
                </p>
              )}
            </Form.Group>
          </div>
        </div>

        <div className={styles.BannerFormmain}>
          <div className="col-lg-6 Mapheit">
            <Form.Group controlId="state.ControlSelect1">
              <Form.Label style={{ color: "#fff", fontSize: "14px" }}>
                Select State <span style={{ color: "white" }}>*</span>
              </Form.Label>
              <Dropdown
                className="mainformcontrol shadow-none w-100"
                value={getValues("state")}
                onChange={(e) =>
                  setValue("state", e.target.value, {
                    shouldValidate: true,
                  })
                }
                options={states}
                placeholder="Select a State"
                filter
              />
              {errors.state && (
                <p
                  className="text-warning position-absolute"
                  style={{ fontSize: "10px" }}
                >
                  {errors.state.message}
                </p>
              )}
            </Form.Group>
          </div>

          <div className="col-lg-6 Mapheit">
            <Form.Group controlId="city.ControlSelect1">
              <Form.Label style={{ color: "#fff", fontSize: "14px" }}>
                Select City <span style={{ color: "white" }}>*</span>
              </Form.Label>
              <Dropdown
                className="mainformcontrol shadow-none w-100"
                value={getValues("city")}
                onChange={(e) =>
                  setValue("city", e.target.value, {
                    shouldValidate: true,
                  })
                }
                options={cities}
                placeholder="Select a City"
                filter
              />
              {errors.city && (
                <p
                  className="text-warning position-absolute"
                  style={{ fontSize: "10px" }}
                >
                  {errors.city.message}
                </p>
              )}
            </Form.Group>
          </div>
        </div>
        {getValues("centresCount") ? (
          <div className={styles.BannerFormmain}>
            <div className="col-lg-12 Mapheit">
              <Form.Group controlId="learning.ControlSelect1">
                <Form.Label style={{ color: "#fff", fontSize: "14px" }}>
                  Select Learning Center{" "}
                  <span style={{ color: "white" }}>*</span>
                </Form.Label>
                <Dropdown
                  className="mainformcontrol shadow-none w-100"
                  value={getValues("learningCentre")}
                  onChange={(e) =>
                    setValue("learningCentre", e.target.value, {
                      shouldValidate: true,
                    })
                  }
                  options={learningCentres}
                  placeholder="Select a Learning Centre"
                  optionValue="id"
                  optionLabel="name"
                  itemTemplate={centreOptionTemplate}
                  filter
                />
                {errors.learningCentre && (
                  <p
                    className="text-warning position-absolute"
                    style={{ fontSize: "10px" }}
                  >
                    {errors.learningCentre.message}
                  </p>
                )}
              </Form.Group>
            </div>
          </div>
        ) : null}

        <div className={styles.BannerFormmaints}>
          <div className="col-lg-6 mx-1 bannermainheight">
            {submittingLead ? (
              <Button disabled variant="primary" className="bannerbtn">
                Submitting <Spinner size="sm" animation="border" />
              </Button>
            ) : (
              <Button variant="warning" type="submit" className="bannerbtny">
                Register
              </Button>
            )}
          </div>
          <div className=" col-lg-6 mx-1 bannermainheight">
            <a
              target="_blank"
              href="https://skills-application.collegevidya.com/"
            >
              <Button variant="dark"  className=" bannermainbtn text-white text-center">
                Sign In <BsArrowRight />
              </Button>
            </a>
          </div>
        </div>
      </div>

      <ToastContainer position="top-end" className="p-3">
        <Toast
          show={showError}
          onClose={() => setShowError(false)}
          delay={3000}
          autohide
          className="d-inline-block m-1"
          bg={"danger"}
        >
          <Toast.Header>
            <strong className="me-auto">Error!</strong>
          </Toast.Header>
          <Toast.Body>{errorStatement}</Toast.Body>
        </Toast>
      </ToastContainer>
    </Form>
  );
};

export default BannerForm;
