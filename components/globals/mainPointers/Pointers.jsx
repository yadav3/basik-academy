import Image from "next/image";
import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import styles from "../mainPointers/Pointers.module.css";

export const Pointers = () => {
  return (
    <section className={`${styles.PointerSec}`}>
      <Container>
        <Row className="my-3 align-items-center">
          <Col md={6}>
            <Image
              width={452}
              height={300}
              src="/about/c15.png"
              alt="myimg"
              className={`${styles.imgSeracr}`}
            />
          </Col>
          <Col md={6}>
            <div className="heading-box">
              <h1>
                What do you <span> get</span>
              </h1>
            </div>
            <ul className={`${styles.FesutresL}`}>
              <li>Work Integrated Bachelor’s Degree</li>
              <li>Certificate by CV Skills Academy on Completion of First Year </li>
              <li>Earn while you Learn</li>
              <li>Apprenticeship/OJT in the Industry</li>
              <li>Remodelled Employment Driven Career Opportunities</li>
              <li>
                Banking & Finance know-how which relates the Entrepreneurial
                Skills
              </li>
              <li>
                Exposure to Tax Planning, Accounts Management, Tax, GST &
                Industrial Law
              </li>
            </ul>
          </Col>
        </Row>
        <Row className="my-5 align-items-center">
          <Col className="order-2 order-md-1" md={6}>
            <div className="heading-box">
              <h1>
                Salient<span> Features</span>
              </h1>
            </div>
            <ul className={`${styles.FesutresL}`}>
              <li>In-depth practical knowledge of the latest financial software. </li>
              <li>Accounting packages like- Tally Prime, Busy, etc.  </li>
              <li>Real-life projects with actual data of industry.</li>
              <li>Regular Workshops & Seminars by industry experts. </li>
              <li>Remodelled Employment Driven Career Opportunities</li>
              <li> Virtual Classrooms </li>
              <li>Faculty pool comprises of-Professional & Practicing C.A., C.S.& C.W.A.</li>
              <li>8,000+Students placed </li>
              <li>Lifetime placement support SALIENT FEATURES </li>
            </ul>
          </Col>
          <Col className="order-1 order-md-2" md={6}>
            <Image
              width={522}
              height={322}
              src="/about/c16.png"
              alt="DegreePhoto"
              className={`${styles.imgSeracr}`}
            />
          </Col>
        </Row>
      </Container>
    </section>
  );
};
