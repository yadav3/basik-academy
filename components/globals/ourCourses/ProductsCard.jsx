import React, { useState } from "react";
import { Button, Carousel } from "react-bootstrap";
import styles from "./OurCourses.module.css";
import Image from "next/image";
import AnchorButton from "../button/AnchorButton";
import FormModal from "../modals/FormModal";

const ProductsCard = (props, { loginForm }) => {
  const [showModal, setShowModal] = useState(false);

  const handleShowModal = () => {
    setShowModal(true);
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };
  return (
    <>
      <section
        className={`${styles.productsSection}  typeCourse globalCarousel homeCarousel`}
      >
        <div className="card myUnstopccard">
          <Image
            className="d-block"
            width={499}
            height={283}
            src={props.CardImg}
            alt="CardImage"
          />
          <div className="card-body">
            <div className="d-flex justify-content-between">
              <p className={`${styles.aboutCourse}`}>{props.Title01}</p>
              <p className={`${styles.aboutCourse}`}>{props.TitleFees}</p>
            </div>
            <h5 className="card-title">{props.Title02}</h5>
            <div className="cardParagraph">{props.Pagragraph}</div>
            <div className={`${styles.starCont}`}>
              <ul className={`${styles.iconStarCont}`}>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
              </ul>
              <div className={`${styles.ratingBox}`}>
                <span>{props.title04}</span> {props.title05}
              </div>
            </div>
            <div className={`${styles.cardFooter}`}>
              <div className="d-flex justify-content-between">
                <span className={`${styles.aboutCourse}`}>{props.NoYear}</span>
                <span className={`${styles.aboutCourse}`}>{props.NoHour}</span>
              </div>
              <span className={`${styles.aboutCourses}`}>{props.whoCanDo}</span>
              <span className={`${styles.aboutCourse}`}>
                {props.whoCanText}
              </span>
              <Button onClick={() => handleShowModal()} className="openModal">
                {" "}
                {props.AncrName}{" "}
              </Button>
            </div>
          </div>
        </div>
      </section>
      <FormModal show={showModal} handleCloseModal={handleCloseModal} />
    </>
  );
};

export default ProductsCard;
