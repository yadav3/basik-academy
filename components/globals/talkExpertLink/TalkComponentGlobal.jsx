import React, { useEffect, useState } from "react";
import styles from "../button/Button.module.css";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

import {
  Navbar,
  Container,
  Nav,
  header,
  Button,
  NavDropdown,
  NavItem,
  Modal,
  Form,
  Row,
} from "react-bootstrap";
import Image from "next/image";
import CustomFormControl from "../CustomFormControl";
import FormModal from "../modals/FormModal";

export const TalkComponentGlobal = (props) => {
  const [showModal, setShowModal] = useState(false);

  const handleShowModal = () => {
    setShowModal(true);
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  useEffect(() => {}, [showModal]);

  return (
    <>
      <section className="talkExpertLink sectionGlobal">
        <div className="container">
          <div className="row">
            <div className="col-md-10 mx-auto">
              <h1>{props.Heading}</h1>
              <a className="talkExBtn anchorbtnStyle" onClick={handleShowModal}>
                Talk to Our Expert
              </a>
            </div>
          </div>
        </div>
      </section>
      <div className="talkSection">
        <FormModal show={showModal} handleCloseModal={handleCloseModal} />
      </div>
    </>
  );
};

export default TalkComponentGlobal;
