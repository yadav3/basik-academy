import React from "react";
import styles from "./Basik.module.css";
import Image from "next/image";
import { FaQuoteLeft } from "react-icons/fa";

export const Custom_card = (props) => {
  return (
    <>
      <div className={`${styles.cutomCard} mb-3`}>
        <span className={`${styles.qutots_xd}`}>
          {" "}
          <FaQuoteLeft />
        </span>
        <p className={`${styles.studInformation}`}>{props.StudewntInfoText}</p>
        <div className={`${styles.btmSection}`}>
          <Image
            src={props.profileImg}
            width="90"
            height="93"
            alt="imgmain"
            className={`${styles.studentProfile}`}
          />
          <div className={`${styles.studentProf}`}>
            <h5>{props.Name}</h5>
            <p>{props.Designation}</p>
          </div>
        </div>
      </div>
    </>
  );
};

export default Custom_card;
