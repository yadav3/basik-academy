import React from "react";
import Image from "next/image";
import styles from "../BsfiSector/BfsiSector.module.css";
import { Col, Container, Row } from "react-bootstrap";
import { Pointers } from "../mainPointers/Pointers";

export const BfsiSector = () => {
  return (
    <>
      <section className={`${styles.Bsfisector}`}>
        <Container>
          {/* <Row className="mt-5 align-items-center">
            <Col md={4}>
              <div className="imgage-hero px-0 d-none d-md-block">
                <Image
                  src="/bfsisector/b1.png"
                  width="400"
                  height="600"
                  alt="value"
                />
              </div>
              <div className="mx-auto px-0 d-block d-md-none">
                <span>
                  <Image
                    src="/bfsisector/b7.png"
                    width="400"
                    height="250"
                    alt="value"
                  />
                </span>
              </div>
            </Col>
            <Col md={8}>
              <div className="heading-box">
                <h1>
                  About <span> BFSI Sector</span>
                </h1>
              </div>
              <p style={{ textAlign: "justify", lineHeight: "26px" }}>
                India has created a favourable environment for the BFSI
                (Banking, Financial Services, and Insurance) sector to thrive.
                The interdependent factors that you mentioned, such as
                government policy, active public/private involvement, robust
                regulatory measures, and technological evolution, have all
                contributed to the growth of the BFSI sector in recent years.
              </p>
              <p style={{ textAlign: "justify", lineHeight: "26px" }}>
                The active involvement of public and private players in the
                sector has also contributed to its growth. Banks, insurance
                companies, and other financial institutions have made
                significant investments in technology, expanded their reach into
                rural areas, and developed innovative products and services to
                cater to the needs of customers.
              </p>
              <p style={{ textAlign: "justify", lineHeight: "26px" }}>
                The regulatory framework in India is robust and has been
                strengthened in recent years with the introduction of new laws
                and regulations. This has helped to build trust among customers
                and investors, which has further fueled the growth of the BFSI
                sector.
              </p>
              <p style={{ textAlign: "justify", lineHeight: "26px" }}>
                Finally, technological evolution has played a significant role
                in the growth of the BFSI sector in India. The widespread
                adoption of mobile phones and the internet has enabled financial
                institutions to reach a larger customer base, offer more
                convenient services, and reduce costs. The emergence of fintech
                companies has also disrupted the traditional BFSI sector,
                forcing incumbents to innovate and adapt to changing customer
                needs.
              </p>
              <p style={{ textAlign: "justify", lineHeight: "26px" }}>
                Overall, India&apos;s favourable business environment, supported
                by government policy, active public/private involvement, robust
                regulatory measures, and technological evolution, has helped the
                BFSI sector to register strong growth in recent years
              </p>
            </Col>
          </Row> */}

          <Row className="mt-5 align-items-center">
            <Col md={4}>
              <Image
                src="/bfsisector/b2.png"
                width="400"
                height="250"
                alt="value"
                style={{ borderRadius: "8px;" }}
              />
            </Col>
            <Col md={8}>
              <div className="heading-box">
                <h1>
                  {" "}
                  About CV <span> Skills Academy </span>
                </h1>
              </div>
              <p style={{ textAlign: "justify", lineHeight: "26px" }}>
                CV Skills Academy (CVSA) is a leading vocational training
                institution in India that provides industry-relevant training
                programs for individuals seeking a career in the BFSI sector.
              </p>
              <p style={{ textAlign: "justify", lineHeight: "26px" }}>
                At CVSA, we understand the importance of skill development in
                today&apos;s fast-paced and competitive world, and we strive to
                bridge the gap between academic knowledge and industry
                requirements through our innovative training programs. Our aim
                is to make quality education accessible to everyone and help
                build a skilled workforce for a better tomorrow.
              </p>
            </Col>
          </Row>

          <Row className="mt-5 align-items-center">
            <Col className="order-2 order-md-1" md={7}>
              <div className="heading-box">
                <h1>
                  Why <span> Us?</span>
                </h1>
              </div>
              <p style={{ textAlign: "justify", lineHeight: "26px" }}>
                Drawing from over two decades of professional experience, CV
                Skills Academy has developed a comprehensive training
                methodology that aims to equip students with both domain and
                platform-specific skills, thereby making them job-ready. Our
                training programs emphasize practical application of theoretical
                knowledge, where students learn through case studies, real-time
                projects, and on-the-job training. Students are encouraged to
                apply their learning to build practical skills, which are
                evaluated by subject matter experts. Upon successful completion
                of the program, students are awarded a certification that
                reflects their expertise in the domain or platform.
              </p>
            </Col>

            <Col className="order-1 order-md-2" md={5}>
              <Image
                src="/bfsisector/b3.png"
                width="400"
                height="400"
                alt="value"
                style={{ borderRadius: "8px;" }}
              />
            </Col>
          </Row>

          <Row className="mt-5 align-items-center">
            <Col md={5}>
              <Image
                src="/bfsisector/b5.png"
                width="511"
                height="350"
                alt="value"
                style={{ borderRadius: "8px;" }}
              />
            </Col>

            <Col md={7}>
              <div className="heading-box">
                <h1>
                  We are the <span> Change Makers</span>
                </h1>
              </div>
              <p style={{ textAlign: "justify", lineHeight: "26px" }}>
                CV Skills Academy’s key aim is the Vocationalisation of the
                Education system with an objective to enhance skills of the
                youth across the country and empower them with livelihood linked
                vocational and skilling programs. We Create, Provide & Foster
                Inspiring Career for Learners... CVSA believes in bridging the
                skill gap as required by Industries.
              </p>
            </Col>
          </Row>
          {/* <Row className="mt-5 align-items-center">
            <Col className="order-2 order-md-1" md={7}>
              <div className="heading-box">
                <h1>
                  Concept <span> & Need</span>
                </h1>
              </div>
              <p style={{ textAlign: "justify", lineHeight: "26px" }}>
                India is the second fastest growing economy in the world. The
                financial sector is growing @ 30% and the demand for financial
                professionals is increasing by leaps and bounds. For every
                Chartered Accountant and MBA (Finance), there are at least 10
                more people required in critical support functions in the
                Finance sector. A study by the Ministry of Human Resources and
                Development forecasts the shortage of Finance professionals by
                more than 2 lakhs in 2025. The industry estimates say that
                nearly 80% of all graduates in India are unemployable since they
                do not have the requisite skill set.
              </p>
              <p>
                While people in the Metros have access to quality education and
                employment opportunities, they are not easily available to those
                residing in the rural and semi-urban areas of the country.
              </p>
              <p>
                The idea is to tap the markets in these areas, especially the B,
                B+ and C category townships. This can be done by providing
                courses by a company which has a pan Indian presence and strong
                Industry alliances.
              </p>
            </Col>

            <Col className="order-1 order-md-2" md={5}>
              <Image
                src="/bfsisector/b6.png"
                width="510"
                height="452"
                alt="value"
                style={{ borderRadius: "8px;" }}
              />
            </Col>
          </Row> */}
          <Row className="mt-5 align-items-center">
            <div className="heading-box">
              <h1 className="text-center">
                {" "}
                Our <span>Strength</span>
              </h1>
            </div>

            <Col md={12}>
              <Image
                src="/bfsisector/b4.png"
                width="1280"
                height="414"
                alt="value"
                style={{ borderRadius: "8px;" }}
              />
            </Col>
            {/* <Col md={12} className="mt-2">
              <p style={{ textAlign: "justify", lineHeight: "26px" }}>
                CV Skills Academy is a leading vocational training institution
                in India, established in 2019 by the founders of Institute of
                Computer & Finance Executives (ICFe), which was founded in 2005.
                ICFe provided vocational training to individuals seeking a
                career in the field of Accounts & Finance, and successfully
                trained over 10,000 learners. More than 8,000 learners went on
                to secure employment with mid to large-sized companies.
              </p>
              <p style={{ textAlign: "justify", lineHeight: "26px" }}>
                CV Skills Academy identified a significant discrepancy between
                the necessary professional skill sets and the current academic
                skill sets. To address this gap, we have dedicated ourselves to
                bridging it in a seamless manner. Our focus is to help students
                acquire industry-relevant skill sets by offering experiential
                learning opportunities. Our programs are designed and delivered
                by industry experts, ensuring that students receive practical
                knowledge and skills that align with the needs of the job
                market.
              </p>
            </Col> */}
          </Row>
        </Container>
      </section>
      <Pointers />
    </>
  );
};
