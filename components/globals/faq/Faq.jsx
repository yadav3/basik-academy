import styles from "./Faq.module.css";
import Image from "next/image";
import Link from "next/link";
import React, { useState } from "react";
import { Nav, NavItem, NavLink, TabContent, TabPane } from "reactstrap";
import classnames from "classnames";
import Accordion from "react-bootstrap/Accordion";

export const Faq = () => {
  const [activeTab, setActiveTab] = useState("Students");

  const toggleTab = (tab) => {
    if (activeTab !== tab) setActiveTab(tab);
  };
  return (
    <>
      <section className={`${styles.contactContainer}`}>
        <div className="container-fluid">
          <div className="row">
            <div className="imgage-hero px-0 d-none d-md-block">
              <Image
                width={1980}
                height={650}
                className="bannerImage"
                src="/contact/faqBanner.png"
                alt="Homebanner1"
              />
            </div>
            <div className="mx-auto px-0 d-block d-md-none">
              <span>
                <Image
                  width={480}
                  height={286}
                  className="bannerImage"
                  src="/contact/faqBanner1.png"
                  alt="Homebanner2"
                />
              </span>
            </div>
          </div>
        </div>
      </section>
      <section className={`${styles.maniherodiv}`}>
        <div className="container mt-4">
          <div className="row">
            <Accordion defaultActiveKey="0" flush>
                  <Accordion.Item eventKey="0">
                    <Accordion.Header>
                      What is the eligibility of the career (Job Assured)
                      courses?
                    </Accordion.Header>
                    <Accordion.Body>
                      Minimum qualification for all the courses is 10+2.
                    </Accordion.Body>
                  </Accordion.Item>
                  <Accordion.Item eventKey="1">
                    <Accordion.Header>
                      Can a Graduate candidate take admission?
                    </Accordion.Header>
                    <Accordion.Body>
                      Yes, a Graduate candidate can certainly take the admission
                      to develop skill-set required by the industry.
                    </Accordion.Body>
                  </Accordion.Item>
                  <Accordion.Item eventKey="2">
                    <Accordion.Header>
                      Is it necessary to have an English medium schooling in
                      10+2?
                    </Accordion.Header>
                    <Accordion.Body>
                      No, Hindi Medium will also do. Basic understanding of
                      English Language is only needed.
                    </Accordion.Body>
                  </Accordion.Item>
                  <Accordion.Item eventKey="3">
                    <Accordion.Header>
                      Is there a particular session or an intake to take the
                      admission?
                    </Accordion.Header>
                    <Accordion.Body>
                      No, enrolments are open round the year. One should enrol
                      ASAP as there are limited batches only.
                    </Accordion.Body>
                  </Accordion.Item>
                  <Accordion.Item eventKey="4">
                    <Accordion.Header>
                      What are the objectives of this course?
                    </Accordion.Header>
                    <Accordion.Body>
                      <ul>
                        <li>
                          ● To provide a judicious mix of skills relating to a
                          profession and appropriate content of General
                          Education.
                        </li>
                        <li>
                          ● To ensure that the students have adequate knowledge
                          and skills, so that they are work ready at each exit
                          point of the programme.
                        </li>
                        <li>
                          ● To provide flexibility to the students by means of
                          pre-defined entry and multiple exit points.
                        </li>
                        <li>
                          ● To integrate NSQF within the undergraduate level of
                          higher education in order to enhance employability of
                          the graduates and meet industry requirements. Such
                          graduates apart from meeting the needs of local and
                          national industry are also expected to be equipped to
                          become part of the global workforce.
                        </li>
                      </ul>
                    </Accordion.Body>
                  </Accordion.Item>
                  <Accordion.Item eventKey="5">
                    <Accordion.Header>
                      How much does the career course cost?
                    </Accordion.Header>
                    <Accordion.Body>
                      Career Courses costs around 6k-7k per month only.
                    </Accordion.Body>
                  </Accordion.Item>
                  <Accordion.Item eventKey="6">
                    <Accordion.Header>
                      What resources do we need to arrange to join & learn?
                    </Accordion.Header>
                    <Accordion.Body>
                      <ul>
                        <li>
                          ● For Classroom training nothing is required as our
                          classrooms are well equipped with high end computers &
                          stable internet connection.
                        </li>
                        <li>
                          ● Smartphones are required to access the e-content.
                        </li>
                      </ul>
                    </Accordion.Body>
                  </Accordion.Item>
                  <Accordion.Item eventKey="7">
                    <Accordion.Header>
                      What is the examination process?
                    </Accordion.Header>
                    <Accordion.Body>
                      <ul>
                        <li>
                          ● For Classroom training examinations will be
                          conducted in the centre only.
                        </li>
                        <li>
                          ● Common / Final exam will be conducted after
                          completion of all the modules and their assessments.
                        </li>
                        <li>
                          ● Student identity validation and surveillance is a
                          must during the exam time.
                        </li>
                        <li>
                          ● Non-submission of assessments in time will be
                          considered as if not attempted.
                        </li>
                      </ul>
                    </Accordion.Body>
                  </Accordion.Item>
                  <Accordion.Item eventKey="8">
                    <Accordion.Header>
                      When & How will I get the job?
                    </Accordion.Header>
                    <Accordion.Body>
                      <ul>
                        <li>
                          ● Post completion of the course you will be directed
                          to the Centralized Placement Cell (CPC)
                        </li>
                        <li>● CPC shall prepare you for the interviews.</li>
                        <li>
                          ● CPC shall share the openings depending on your
                          overall performance & shall align your interviews.
                        </li>
                      </ul>
                    </Accordion.Body>
                  </Accordion.Item>
                  <Accordion.Item eventKey="9">
                    <Accordion.Header>
                      How will my career progress?
                    </Accordion.Header>
                    <Accordion.Body>
                      <ul>
                        <li>
                          ● Entry Level Accountant (Junior/Assistant
                          Accountant/Executive) : First 2 Years
                        </li>
                        <li>● Accounts/Finance Executive : Next 2 Years</li>
                        <li>
                          ● Sr. Accountant/Sr. Finance Assistant : Next 3-4
                          Years
                        </li>
                        <li>
                          ● Deputy Manager - Accounts/Finance : Next 5-8 Years
                        </li>
                        <li>● Manager - Accounts/Finance : Next 8-10 Years</li>
                      </ul>
                    </Accordion.Body>
                  </Accordion.Item>
                  <Accordion.Item eventKey="10">
                    <Accordion.Header>
                      Can I take online classes?
                    </Accordion.Header>
                    <Accordion.Body>
                      <ul>
                        <li>
                          ● For Career Courses the curriculum is designed on
                          Full-Time Mode
                        </li>
                        <li>
                          ● In case of absenteeism one may refer to the recorded
                          video at LMS
                        </li>
                        <li>
                          ● For Non-Career Courses one may opt for online
                          delivery
                        </li>
                      </ul>
                    </Accordion.Body>
                  </Accordion.Item>
                  <Accordion.Item eventKey="11">
                    <Accordion.Header>
                      What usually is the size of a batch?
                    </Accordion.Header>
                    <Accordion.Body>
                      <ul>
                        <li>
                          ● At CV Skills Academy, we maintain 1:1 ratio of
                          Learner & Machine
                        </li>
                        <li>
                          ● Maximum 15 students in a batch (Offline Programs)
                        </li>
                      </ul>
                    </Accordion.Body>
                  </Accordion.Item>
                  <Accordion.Item eventKey="12">
                    <Accordion.Header>
                      Are these career courses certified by anybody?
                    </Accordion.Header>
                    <Accordion.Body>
                      <ul>
                        <li>● These courses are industry integrated</li>
                        <li>
                          ● Developed according to the need of the industry
                        </li>
                        <li>
                          ● Inputs taken by the industry experts to fulfil the
                          gap relating to unskilled manpower
                        </li>
                        <li>
                          ● Accredited by the panel of experts from the Accounts
                          & Finance Industry
                        </li>
                      </ul>
                    </Accordion.Body>
                  </Accordion.Item>
                  <Accordion.Item eventKey="13">
                    <Accordion.Header>
                      What are the modes of payments to take admission?
                    </Accordion.Header>
                    <Accordion.Body>
                      <ul>
                        <li>● Learner can pay in any mode except cash</li>
                        <li>
                          ● We prefer the payments on our official website only
                        </li>
                        <li>
                          ● Inputs taken by the industry experts to fulfil the
                          gap relating to unskilled manpower
                        </li>
                        <li>
                          ● Learner can pay through Net Banking, Debit/Credit
                          Cards, Wallets or UPI
                        </li>
                      </ul>
                    </Accordion.Body>
                  </Accordion.Item>
                  <Accordion.Item eventKey="14">
                    <Accordion.Header>
                      What is the maximum age for taking the admission?
                    </Accordion.Header>
                    <Accordion.Body>
                      <ul>
                        <li>
                          ● For Career Courses, the maximum age is 28 years
                        </li>
                        <li>● For Non-Career Courses there is no age limit</li>
                      </ul>
                    </Accordion.Body>
                  </Accordion.Item>
                  <Accordion.Item eventKey="15">
                    <Accordion.Header>Why CVSA?</Accordion.Header>
                    <Accordion.Body>
                      <ul>
                        <li>
                          ● With over 2 decades of professional experience, CV
                          Skills Academy has designed it’s training methodology
                          aiming toward equipping the students with domain &
                          platform skills thus enabling them to become
                          employment ready.
                        </li>
                        <li>
                          ● Student learns the subject, through case studies &
                          real time projects & on job training candidates
                          translates his learning to application skill which is
                          assessed by the subject matter expert and finally the
                          candidate is certified.
                        </li>
                      </ul>
                    </Accordion.Body>
                  </Accordion.Item>
                  <Accordion.Item eventKey="16">
                    <Accordion.Header>
                      What are the Job Prospects after the completion of the
                      course?
                    </Accordion.Header>
                    <Accordion.Body>
                      <ul>
                        <li>
                          ● The program aims at providing education with the
                          idea of building specific job skills in students, so
                          that they can gainfully participate in accelerating
                          the growth of India’s economy and also serve the
                          industries better.
                        </li>
                        <li>
                          ● For an entry level accountant, an average annual
                          salary of about 2-4 lakhs is expected. The job is to
                          maintain basic accounting entries on a daily basis,
                          taking quotations from vendors.
                        </li>
                      </ul>
                    </Accordion.Body>
                  </Accordion.Item>
                  <Accordion.Item eventKey="17">
                    <Accordion.Header>
                      What help will be provided for JOB/Placement in the future
                      course of time?
                    </Accordion.Header>
                    <Accordion.Body>
                      <ul>
                        <li>● Job Assistance is for the lifetime</li>
                        <li>
                          ● With the time, one gains experience & there’s always
                          a requirement for the experience candidates in the
                          market
                        </li>
                      </ul>
                    </Accordion.Body>
                  </Accordion.Item>
                  <Accordion.Item eventKey="18">
                    <Accordion.Header>
                      What are the highlights of the courses?
                    </Accordion.Header>
                    <Accordion.Body>
                      <ul>
                        <li>● LMS</li>
                        <li>● Online Classes (Ease of Learning)</li>
                        <li>● Practical Assignments on each topic</li>
                        <li>
                          ● Special Attraction (Exclusivity): Video Recording
                          Solutions for the Assignments
                        </li>
                        <li>● Online Exams</li>
                        <li>● Course Progress can be tracked</li>
                        <li>
                          ● Raise The Doubts Through Ticketing System on LMS
                        </li>
                        <li>
                          ● Certifications (Post Completion of the Program)
                        </li>
                        <li>
                          ● Re-subscribe to LMS with only Rs. 99 (validity 30
                          days)
                        </li>
                      </ul>
                    </Accordion.Body>
                  </Accordion.Item>
                  <Accordion.Item eventKey="19">
                    <Accordion.Header>What is B.Voc?</Accordion.Header>
                    <Accordion.Body>
                      <p>
                        B.Voc stands for Bachelors in Vocational studies which
                        is an emerging course in our country. This course aims
                        at allowing an individual to acquire skills for a
                        particular trade. These are non-academic. The course
                        deals with application based studies rather than
                        depending on theoretical knowledge completely.
                        Vocational studies cover a wide range of fields. Some of
                        these fields include health care, creative fields like
                        graphic designing and web designing, food technology. It
                        is also provided in skills trade including plumbing,
                        automotive repair and many more.
                      </p>
                      <p>
                        The course specializes on providing practical training
                        and hence can be regarded as a skill development degree.
                      </p>
                    </Accordion.Body>
                  </Accordion.Item>
                  <Accordion.Item eventKey="20">
                    <Accordion.Header>
                      What is vocational training?
                    </Accordion.Header>
                    <Accordion.Body>
                      <ul>
                        <li>
                          ● Vocational courses are an educational discipline
                          that enables individuals to acquire skills that are
                          required for a particular trade.
                        </li>
                        <li>
                          ● Vocational courses are traditionally non-academic
                          and are completely related to a specific trade,
                          occupation or vocation.
                        </li>
                        <li>
                          ● These courses are designed to impart application
                          based study where theoretical aspects are not studied
                          independently.
                        </li>
                      </ul>
                    </Accordion.Body>
                  </Accordion.Item>
                  <Accordion.Item eventKey="21">
                    <Accordion.Header>
                      Is there any difference between enrolling at a Franchisee
                      Centre or Company Owned Centre?
                    </Accordion.Header>
                    <Accordion.Body>
                      <ul>
                        <li>
                          ● At CVSA, we believe in imparting quality education &
                          having a strong monitoring as an organization.
                        </li>
                        <li>
                          ● All faculties are well trained & certified by
                          central governing body.
                        </li>
                        <li>
                          ● x`LMS is purely designed & developed by the
                          organization.
                        </li>
                        <li>
                          ● Examinations are conducted & valuated by the
                          organization.
                        </li>
                        <li>
                          ● Certifications are also awarded by the organization.
                        </li>
                      </ul>
                    </Accordion.Body>
                  </Accordion.Item>
                </Accordion>
          </div>
        </div>
      </section>
    </>
  );
};

export default Faq;
