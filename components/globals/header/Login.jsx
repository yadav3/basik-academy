import React from "react";
import { Modal, Form, Button } from "react-bootstrap";
import Image from "next/image";
import styles from "./Header.module.css";
import { useForm } from "react-hook-form";

export const Login = (props) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = (data) => {};

  return (
    <>
      <Modal.Header closeButton className="text-center">
        <Image
          width={150}
          height={40}
          src="/Skills-academy_logo.png"
          alt="login"
        />
        <h1>Sign In To Your Account</h1>
        <Modal.Title>
          Do not have an account?
          <button
            className="bg-transparent mt-0"
            onClick={() => props.changeToRegister()}
          >
            SIGN UP
          </button>
        </Modal.Title>
      </Modal.Header>
      <Form className="loginRegister" onSubmit={handleSubmit(onSubmit)}>
        <Form.Floating className="mb-3">
          <Form.Control
            id="floatingInputCustom"
            type="email"
            placeholder="name@example.com"
            {...register("email", {
              required: true,
              pattern: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
            })}
          />
          <label htmlFor="floatingInputCustom">
            Enter your Email Address <span className="text-danger">*</span>
          </label>
          {errors.email && (
            <span className="text-danger">
              Please enter a valid email Address
            </span>
          )}
        </Form.Floating>
        <Form.Floating>
          <Form.Control
            id="floatingPasswordCustom"
            type="password"
            placeholder="Enter your Password"
            {...register("password", {
              required: true,
              minLength: 6,
            })}
          />
          <label htmlFor="floatingPasswordCustom">
            Enter your Password <span className="text-danger">*</span>
          </label>
          {errors.password && (
            <span className="text-danger">
              Password must be at least 6 characters long
            </span>
          )}
        </Form.Floating>
        <Button variant="primary" type="submit">
          Login
        </Button>
        <div className={`${styles.termCondition}`}>
          <div className={`${styles.term01}`}>
            By clicking &quot;signup&quot; you agree to our
          </div>
          <a href="#" className={`${styles.term02}`}>
            Terms of Service
          </a>
          <div className={`${styles.term03}`}> &amp; </div>
          <a href="#" className={`${styles.term04}`}>
            Privacy Policy
          </a>
        </div>
      </Form>
    </>
  );
};
