const express = require("express");
const fileUpload = require("express-fileupload");
const https = require("https");
const fs = require("fs");
const axios = require("axios");

// Certificate
const privateKey = fs.readFileSync(
    "/opt/psa/var/certificates/scfwXgCFJ",
    "utf8"
);
const certificate = fs.readFileSync(
    "/opt/psa/var/certificates/scfwXgCFJ",
    "utf8"
);

const credentials = {
    key: privateKey,
    cert: certificate,
};

const app = express();

app.use(express.json({ limit: "200mb" }));
app.use(express.urlencoded({ extended: true }));
app.use(fileUpload());

app.use((_request, response, next) => {
    response.setHeader("Access-Control-Allow-Origin", "*");
    response.setHeader(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    response.setHeader(
        "Access-Control-Allow-Methods",
        "GET, POST, PATCH, DELETE, PUT, OPTIONS"
    );
    next();
});

const httpsServer = https.createServer(credentials, app);

app.get("/", (request, response) => {
    response.status(200).send("Server is running");
});

app.post("/leads", async (request, response) => {
    if (!request.body) {
        return response.status(400).send("Request is invalid");
    }

    const fullName = request.body.fullName;
    const email = request.body.email;
    const dob = request.body.dob;
    const mobileNumber = request.body.mobileNumber;
    const state = request.body.state;
    const pincode = request.body.pincode;
    const message = request.body.message;

    try {
        await sendTemplateMail(
            499,
            [
                {
                    email: "atulverma@blackboardindia.com",
                },
                {
                    email: "atul@blackboardindia.com",
                },
                {
                    email: "swetk@blackboardindia.com",
                },
            ],
            {
                FIRSTNAME: fullName,
                EMAIL: email,
                INSTITUTECODE: dob,
                PHONE: mobileNumber,
                ADDRESS: state,
                PINCODE:pincode,
                MESSAGE: message,
            }
        );

        response.status(200).send("Lead Added");
    } catch (error) {
        response.status(400).send(error.response.data);
    }
});

app.post("/stepsLeads", async (request, response) => {
    if (!request.body) {
        return response.status(400).send("Request is invalid");
    }

    const fullName = request.body.fullName;
    const email = request.body.email;
    const dob = request.body.dob;
    const mobileNumber = request.body.mobileNumber;
    const state = request.body.state;
    const pincode = request.body.pincode;
    const message = request.body.message;
    const QualificationHighest = request.body.QualificationHighest;
    const Coursessuitsyou = request.body.Coursessuitsyou;
    const salaryExpectations = request.body.salaryExpectations;
    const levelOfRoles = request.body.levelOfRoles;
    const specificCompany = request.body.specificCompany;


    try {
        await sendTemplateMail(
            499,
            [
                {
                    email: "atulverma@blackboardindia.com",
                },
                {
                    email: "atul@blackboardindia.com",
                },
                {
                    email: "swetk@blackboardindia.com",
                },
            ],
            {
                FIRSTNAME: fullName,
                EMAIL: email,
                INSTITUTECODE: dob,
                PHONE: mobileNumber,
                ADDRESS: state,
                PINCODE:pincode,
                MESSAGE: message,
                CVHELP: QualificationHighest,
                INSTITUTE: Coursessuitsyou,
                EXPERIENCE: salaryExpectations,
                REFERNAME: levelOfRoles,
                OTHERCVHELP: specificCompany,
            }
        );

        response.status(200).send("Lead Added");
    } catch (error) {
        response.status(400).send(error.response.data);
    }
});

app.post("/partnerLeads", async (request, response) => {
    if (!request.body) {
        return response.status(400).send("Request is invalid");
    }

    const fullName = request.body.fullName;
    const email = request.body.email;
    const occupation = request.body.occupation;
    const mobileNumber = request.body.mobileNumber;
    const state = request.body.state;
    const pincode = request.body.pincode;
    const message = request.body.message;

    try {
        await sendTemplateMail(
            499,
            [
                {
                    email: "atulverma@blackboardindia.com",
                },
                {
                    email: "atul@blackboardindia.com",
                },
                {
                    email: "swetk@blackboardindia.com",
                },
            ],
            {
                FIRSTNAME: fullName,
                EMAIL: email,
                WORKING: occupation,
                PHONE: mobileNumber,
                ADDRESS: state,
                PINCODE: pincode,
                MESSAGE: message,
            }
        );

        response.status(200).send("Lead Added");
    } catch (error) {
        response.status(400).send(error.response.data);
    }
});

const sendTemplateMail = async function (templateId, to, params) {
    const data = {
        to: to,
        templateId: templateId,
        params: params,
    };

    const config = {
        method: "post",
        url: "https://api.sendinblue.com/v3/smtp/email/",
        headers: {
            Accept: "application/json",
            "api-key": "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",
            "Content-Type": "application/json",
        },
        data: data,
    };

    const mailStatus = await axios(config);
    return mailStatus;
};

httpsServer.listen(4000, () => {
    console.log("Server is running on port " + 4000);
});