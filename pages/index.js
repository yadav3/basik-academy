import Header from "../components/globals/header/Header";
import PartnerCounter from "../components/globals/partnerCounter/PartnerCounter";
import TalkComponentGlobal from "../components/globals/talkExpertLink/TalkComponentGlobal";
import Blog from "../components/globals/blog/Blog";
import Footer from "../components/globals/footer/Footer";
import MasterVideo from "../components/pagescomponents/faculty/MasterVideo";
import { BfsiSector } from "../components/globals/BsfiSector/BfsiSector";
import { Banner } from "../components/globals/banner/Banner";
import BasikCustomTab from "../components/globals/basiksPride/BasikCustomTab";

export default function Home() {
  return (
    <>
      <Header />
      <Banner />
      <BfsiSector />
      <PartnerCounter />
      <TalkComponentGlobal
        Heading="Transform your future with College Vidya education
          get register and celebrate the life."
        buttonName="Talk to Our Expert"
      />
      <section className="masterVideo">
        <MasterVideo />
      </section>
      <section className="basikCont sectionGlobal">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <BasikCustomTab />
            </div>
          </div>
        </div>
      </section>
      <Blog />
      <Footer />
    </>
  );
}
