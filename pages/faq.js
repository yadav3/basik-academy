import React from "react";
import Header from "../components/globals/header/Header";
import Faq from "../components/globals/faq/Faq";
import Footer from "../components/globals/footer/Footer";

export default function faq() {
  return (
    <>
      <Header />
      <Faq />
      <Footer />
    </>
  );
}
