import React from "react";
import Header from "../components/globals/header/Header";
import Footer from "../components/globals/footer/Footer";
import styles from "../components/pagescomponents/about/About.module.css";
import IndustryFunction from "../components/globals/industry/Industry";

export default function aboutIndustry() {
  return (
    <>
      <Header />
      <section className={`${styles.aboutBanr}`}>
        <h1>
          <b>College Vidya Skills Academy</b>
        </h1>
        <h5>
          <b>About Industry</b>
        </h5>
      </section>
      <IndustryFunction />
      <Footer />
    </>
  );
}
