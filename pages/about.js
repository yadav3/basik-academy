import React from "react";
import Header from "../components/globals/header/Header";
import Footer from "../components/globals/footer/Footer";
import styles from "../components/pagescomponents/about/About.module.css";
import Section03 from "../components/pagescomponents/about/Section03";
import Section06 from "../components/pagescomponents/about/Section06";
import Section05 from "../components/pagescomponents/about/section05/Section05";

export default function about() {
  return (
    <>
      <Header />
      <section className={`${styles.aboutBanr}`}>
        <h1>
          <b>College Vidya Skills Academy</b>
        </h1>
        <h5>
          <b>Kare apne sapno ko sakar</b>
        </h5>
      </section>
      {/* <section className="sectionGlobal aboutHding">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <HeadingContent
                heading="We are <span>College Vidya Skills Academy</span>"
                pagragraph="<p>
                College Vidya Skills Academy is India’s most progressive vocational skill academy founded in Yr. 2019 by the founders of Institute of Computer & Finance Executives (ICFe) which was founded in Yr.2005, imparted Vocational Training to aspirants looking for aspiring career in the field of Accounts & Finance. Over 10000 Learner were trained and 8000 Learners were employed in mid and large size companies.
                </p>
                <p>
                College Vidya Skills Academy observed a striking disparity between the required professional
                 skill sets & the existing academic skill sets & strived to seamlessly bridge the gap.
                  We help students develop industry-relevant skill sets through experiential 
                  learning – with programs designed & delivered by industry experts.
                </p>

                <p> <h4><b>And You know this</b> </h4>
                  <p>
                    Experiential learning is the key to transforming careers.
                    Ever Since the goal has been to bridge the gap between the
                    Industry demand and Existing skill set, by using
                    experiential learning to train candidates & help them
                    become competent leaders
                  </p></p>
                  
                  "
              />
            </div>
          </div>
        </div>
      </section> */}
      <Section03 />

      <Section06 />
      <Section05 />

      <Footer />
    </>
  );
}
