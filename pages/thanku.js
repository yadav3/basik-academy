import { Col, Container, Row } from "react-bootstrap";
import { AiOutlineCheck } from "react-icons/ai";
import Header from "../components/globals/header/Header";
import Footer from "../components/globals/footer/Footer";
import Link from "next/link";

export default function ThankYou() {
  return (
    <>
      <Header />
      <section
        style={{
          display: "flex",
          alignItems: "center",
          height: "99vh",
          paddingTop: "3rem",
        }}
      >
        <Container>
          <Row className="align-items-center h-100">
            <Col>
              <div className="text-center col-sm-12 align-items-center justify-content-center">
                <h2 className="Thanks">Thank You!</h2>
                <span className="myicons">
                  <AiOutlineCheck />
                </span>
                <h6 className="thanksSha">
                  Thank you for sharing your details!
                </h6>
                <p className="myPara">
                  One of our experts will be connecting with you shortly!
                </p>
                <Link href="/">
                  <a style={{ color: "#002EAF" }}>
                    <p>Explore More</p>
                  </a>
                </Link>
              </div>
            </Col>
          </Row>
        </Container>
      </section>
      <Footer />
    </>
  );
}
