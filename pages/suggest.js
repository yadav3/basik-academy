import Ottaheader from "../components/Otta/Ottaheader";
import { StepsForm } from "../components/Otta/StepsForm";

const Test = () => {
  return (
    <>
      <Ottaheader />
      <StepsForm />
    </>
  );
};

export default Test;
