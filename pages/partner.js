import React from "react";
import Header from "../components/globals/header/Header";
import Footer from "../components/globals/footer/Footer";
import Partner from "../components/globals/patner/Partner";

export default function partner() {
  return (
    <>
      <Header />
      <Partner />
      <Footer />
    </>
  );
}
