import axios from "axios";

export const getCourseTypesAPIKey = () => {
  return axios.get(
    process.env.NEXT_PUBLIC_UNIVERSITY_ADMISSION_URL +
      `settings/course-types?key=${"9f6289ee-b29e-11ed-a54b-f23c921aa8b0"}`
  );
};

export const getCourseAPIKey = (courseTypeKey) => {
  return axios.get(
    process.env.NEXT_PUBLIC_UNIVERSITY_ADMISSION_URL +
      `settings/courses?key=${"9f6289ee-b29e-11ed-a54b-f23c921aa8b0"}&course_type=${courseTypeKey}`
  );
};

export const getSubCourses = (courseAPIKey) => {
  return axios.get(
    process.env.NEXT_PUBLIC_UNIVERSITY_ADMISSION_URL +
      `settings/sub_courses?course=${courseAPIKey}`
  );
};

export const getStates = () => {
  return axios.get(
    process.env.NEXT_PUBLIC_UNIVERSITY_ADMISSION_URL + `regions`
  );
};

export const getCities = (state) => {
  return axios.get(
    process.env.NEXT_PUBLIC_UNIVERSITY_ADMISSION_URL + `regions?state=${state}`
  );
};

export const getLearningCenters = (state, city) => {
  const formData = new FormData();
  formData.append("state", state);
  formData.append("city", city);

  return axios.post(
    process.env.NEXT_PUBLIC_UNIVERSITY_ADMISSION_URL + `cv-skills/users`,
    formData
  );
};

export const generateStudentID = (data) => {
  return axios.post(
    process.env.NEXT_PUBLIC_UNIVERSITY_ADMISSION_URL + `cv-skills/lead`,
    data
  );
};
